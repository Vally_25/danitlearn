/*
можливість виконання коду до завершення дій користувача. наприклад, запити до сервера, робота з файлами, 
які можуть займати тривалий час, але при цьому не блокують інші частини програми.

У синхронному коді JavaScript виконується послідовно: одна операція завершується, 
потім починається інша. Але у асинхронному коді інші операції можуть далі виконуватись
поки код чекає на відповідь асинхронного коду.

*/
document.getElementById('findIpButton').addEventListener('click', async () => {
    try {
        const ipResponse = await axios.get('https://api.ipify.org/?format=json');
        const ip = ipResponse.data.ip;

        const locationResponse = await axios.get(`http://ip-api.com/json/${ip}`);
        const locationData = locationResponse.data

        const countryResponse = await axios.get(`http://api.geonames.org/countryInfoJSON?country=${locationData.countryCode}&username=vally25`);
        const continent = countryResponse.data.geonames[0].continentName;

        //Геонеймс не дає назви районів, тому вивід відмічено коментарем:
        const districtResponse = await axios.get(`http://api.geonames.org/findNearbyPlaceNameJSON?lat=${locationData.lat}&lng=${locationData.lon}&username=vally25`);
        const district = districtResponse.data.geonames[0].adminName1;

        const infoContainer = document.getElementById('ipInfo');
        infoContainer.innerHTML = `
            <p>Континент: ${continent}</p>
            <p>Країна: ${locationData.country}</p>
            <p>Регіон: ${locationData.regionName}</p>
            <p>Місто: ${locationData.city}</p>
            <p>Район: ${`Soborniy ${district/*тут вивів руками назву району, бо максимально,
        що виводить геонеймс - міста*/}`}</p>
        `;
    } catch (error) {
        console.error('Помилка під час виконання запиту:', error);
    }
});