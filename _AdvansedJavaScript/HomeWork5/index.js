'use strict';

async function getUsers() {
  try {
    const response = await axios.get('https://ajax.test-danit.com/api/json/users');
    return response.data;
  } catch (error) {
    console.error('Error fetching users:', error);
  }
}

async function getPosts() {
  try {
    const response = await axios.get('https://ajax.test-danit.com/api/json/posts');
    return response.data;
  } catch (error) {
    console.error('Error fetching posts:', error);
  }
}

function displayPosts(users, posts) {
  const postsContainer = document.getElementById('posts-container');

  posts.forEach(post => {
    const user = users.find(user => user.id === post.userId);
    if (user) {
      const card = new Card(post.title, post.body, user.name, user.email, post.id);
      postsContainer.appendChild(card.render());
    }
  });

  setTimeout(() => {
    document.getElementById('preloader').style.display = 'none';
    postsContainer.style.display = 'block';
  }, 3000);
}

class Card {
  constructor(title, body, userName, userEmail, postId, userId) {
    this.title = title;
    this.body = body;
    this.userName = userName;
    this.userEmail = userEmail;
    this.postId = postId;
    this.userId = userId;
  }

  render() {
    const cardElement = document.createElement('div');
    cardElement.classList.add('card');
    cardElement.id = `post-${this.postId}`;

    const titleElement = document.createElement('h2');
    titleElement.textContent = this.title;

    const bodyElement = document.createElement('p');
    bodyElement.textContent = this.body;

    const userInfoElement = document.createElement('p');
    userInfoElement.textContent = `Posted by ${this.userName} (${this.userEmail})`;

    const editButton = document.createElement('button');
    editButton.style.backgroundImage = 'url("./pencil-outline.png")';
    editButton.addEventListener('click', this.editCard.bind(this));

    const deleteButton = document.createElement('button');
    deleteButton.style.backgroundImage = 'url("./trash-can.png")';
    deleteButton.addEventListener('click', this.confirmDelete.bind(this));

    cardElement.appendChild(titleElement);
    cardElement.appendChild(bodyElement);
    cardElement.appendChild(userInfoElement);
    cardElement.appendChild(editButton);
    cardElement.appendChild(deleteButton);

    return cardElement;
  }

  async editCard() {
    const newTitle = prompt('Enter new title:', this.title);
    const newBody = prompt('Enter new body:', this.body);

    if (newTitle !== null && newBody !== null) {
      try {
        const response = await axios.put(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
          title: newTitle,
          body: newBody,
        }, {
          headers: {
            'Content-Type': 'application/json'
          }
        });

        if (response.status === 200) {
          this.title = newTitle;
          this.body = newBody;
          this.updateCard();
        } else {
          throw new Error('Failed to update post');
        }
      } catch (error) {
        console.error('Error updating post:', error);
      }
    }
  }

  updateCard() {
    const cardElement = document.getElementById(`post-${this.postId}`);
    if (cardElement) {
      cardElement.querySelector('h2').textContent = this.title;
      cardElement.querySelector('p').textContent = this.body;
    }
  }

  async confirmDelete() {
    const confirmDelete = window.confirm(`Are you sure you want to delete this post with id = ${this.postId}?`);
    if (confirmDelete) {
      await this.deletePost();
    }
  }

  async deletePost() {
    try {
      const response = await axios.delete(`https://ajax.test-danit.com/api/json/posts/${this.postId}`);
      if (response.status === 200) {
        this.removeCard();
      } else {
        throw new Error('Failed to delete post');
      }
    } catch (error) {
      console.error('Error deleting post:', error);
    }
  }

  removeCard() {
    const cardElement = document.getElementById(`post-${this.postId}`);
    if (cardElement) {
      cardElement.remove();
    }
  }
}

async function createPost() {
  const title = prompt('Enter post title:');
  const body = prompt('Enter post body:');

  if (title && body) {
    try {
      const response = await axios.post('https://ajax.test-danit.com/api/json/posts', {
        title,
        body,
        userId: 1,
      }, {
        headers: {
          'Content-Type': 'application/json'
        }
      });

      if (response.status === 200) {
        const newPost = response.data;
        const users = await getUsers();
        displayNewPost(newPost, users);
      } else {
        throw new Error('Failed to create post');
      }
    } catch (error) {
      console.error('Error creating post:', error);
    }
  }
}

function displayNewPost(post, users) {
  const user = users.find(user => user.id === post.userId);
  if (user) {
    const postsContainer = document.getElementById('posts-container');

    const card = new Card(post.title, post.body, user.name, user.email, post.id);
    postsContainer.prepend(card.render());
  }
}

document.addEventListener('DOMContentLoaded', async () => {
  const users = await getUsers();
  const posts = await getPosts();

  displayPosts(users, posts);

  const createButton = document.getElementById('create-post');
  createButton.addEventListener('click', createPost);
});
