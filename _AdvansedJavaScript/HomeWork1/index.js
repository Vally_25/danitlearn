/*
    1) Прототипне насліжування є механізмом, за допомогою якого об'єкти можуть успадковувати 
    властивості та методи від інших об'єктів. Такі об'єкти називаються прототипами.
    кожен об'єкт у джаваскрипті має посилання на прототип, і якщо щось знаходиться не в самому
    об'єкті, то ДЖЕЕС шукає його у прототипі цього об'єкту.

    2) Щоб він знав, звідки наслідувати властивості батьківського класу
*/
class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    // Геттери
    get name() {
        return this._name;
    }
    get age() {
        return this._age;
    }
    get salary() {
        return this._salary;
    }
    // Сеттери
    set name(newName) {
        this._name = newName;
    }
    set age(newAge) {
        this._age = newAge;
    }
    set salary(newSalary) {
        this._salary = newSalary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    get lang() {
        return this._lang;
    }
    get salary() {
        return this._salary * 3;
    }
}
let John = new Programmer("John", 30, 2000, ["JavaScript", "Python"]);
let Bob = new Programmer("Bob", 25, 6000, ["Java", "C++"]);

// Виведення об'єктів у консоль
console.log(John);
console.log(Bob);