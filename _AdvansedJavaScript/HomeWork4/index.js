/*
Asynchronous JavaScript and XML) - це технологія, що дозволяє веб-сторінці здійснювати обмін даними 
з сервером без необхідності перезавантаження всієї сторінки. Можна перезавантажувати або змінювати 
частковий зміст сторінки не перезавантажуючи її всю. Приклад: будь-який інтернет-магазин та кнопка
"показати більше"
*/
"use strict";

const filmContainer = document.querySelector('.filmContainer');
const filmCard = document.createElement('div');
filmCard.classList.add('filmCard');

const mainUrl = 'https://ajax.test-danit.com/api/swapi/films/';
const charUrl = 'https://ajax.test-danit.com/api/swapi/people/'

axios.get(mainUrl)
  .then(response => {
    if (response.status === 200) {

      const films = response.data;

      films.forEach(film => {

        renderFilmCard(film.id, film.name, film.openingCrawl).then(charactersContainer => {
          renderFilmChar(film.characters, charactersContainer)
            .then(() => {
              charactersContainer.querySelector('.loader-container').style.display = 'none';
            });
        });
      })
    }
  })
  .catch(error => {
    console.error('Помилка під час виконання запиту:', error);
  });

function renderFilmCard(filmId, filmName, filmOpenCrawl) {
  return new Promise((resolve, reject) => {

    const filmCard = document.createElement('div');
    filmCard.classList.add('filmCard');

    const filmTitle = document.createElement('h2');
    filmTitle.textContent = `Episode ${filmId}: ${filmName}`;
    filmCard.appendChild(filmTitle);

    const openCrawl = document.createElement('div');
    openCrawl.classList.add('film-description-container');
    openCrawl.textContent = filmOpenCrawl;
    filmCard.appendChild(openCrawl);

    const charactersContainer = document.createElement('ul');
    const loaderContainer = document.createElement('div');
    loaderContainer.classList.add('loader-container');
    const loader = document.createElement('div');
    loader.classList.add('spinner');
    loaderContainer.appendChild(loader);
    charactersContainer.appendChild(loaderContainer);
    
    filmCard.appendChild(charactersContainer);

    filmContainer.appendChild(filmCard);

    openCrawl.addEventListener('click', () => {
      openCrawl.classList.toggle('expanded');
    });
    
    resolve(charactersContainer);
  });
}

function renderFilmChar(charactersUrls, charactersContainer) {
  const charactersPromises = charactersUrls.map(url => axios.get(url)
    .then(response => response.data)

    .then(character => {
      
      const listItem = document.createElement('li');
      listItem.textContent = character.name;
      charactersContainer.appendChild(listItem);
    })

    .catch(error => {
      console.error(`Помилка під час отримання персонажа з URL ${url}:`, error);
    })
  );
  return Promise.all(charactersPromises);
}
