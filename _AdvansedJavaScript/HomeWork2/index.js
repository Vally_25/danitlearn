/*
    - при роботі з зовнішними ресурсами
    - при використанні фреймворків або бібліотек
    - при взаємодії з користувачем
    - коли ти впевнений, що код точно викликає помилку
*/
const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];


const root = document.createElement("div");
root.id = "root";
const ul = document.createElement("ul");

books.forEach(book => {
    try {
        if (!book.author || !book.name || !book.price) {
            throw new Error(`У книзі ${book.name} втрачений аргумент`);
        }

        const li = document.createElement("li");
        li.textContent = `${book.author} - ${book.name} (${book.price} грн)`;
        ul.appendChild(li);
    } catch (error) {
        console.error(error.message);
    }
});

root.appendChild(ul);
document.body.appendChild(root);