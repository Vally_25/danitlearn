"use strict";

const DATA = [
    {
        "first name": "Олексій",
        "last name": "Петров",
        photo: "./img/trainers/trainer-m1.jpg",
        specialization: "Басейн",
        category: "майстер",
        experience: "8 років",
        description:
            "Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
    },
    {
        "first name": "Марина",
        "last name": "Іванова",
        photo: "./img/trainers/trainer-f1.png",
        specialization: "Тренажерний зал",
        category: "спеціаліст",
        experience: "2 роки",
        description:
            "Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
    },
    {
        "first name": "Ігор",
        "last name": "Сидоренко",
        photo: "./img/trainers/trainer-m2.jpg",
        specialization: "Дитячий клуб",
        category: "інструктор",
        experience: "1 рік",
        description:
            "Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
    },
    {
        "first name": "Тетяна",
        "last name": "Мороз",
        photo: "./img/trainers/trainer-f2.jpg",
        specialization: "Бійцівський клуб",
        category: "майстер",
        experience: "10 років",
        description:
            "Тетяна є експертом в бойових мистецтвах. Вона проводить тренування для професіоналів і новачків. Її підхід до навчання допомагає спортсменам досягати високих результатів.",
    },
    {
        "first name": "Сергій",
        "last name": "Коваленко",
        photo: "./img/trainers/trainer-m3.jpg",
        specialization: "Тренажерний зал",
        category: "інструктор",
        experience: "1 рік",
        description:
            "Сергій фокусується на роботі з фітнесом та кардіотренуваннями. Він вдосконалив свої методики протягом багатьох років. Його учні завжди в формі та енергійні.",
    },
    {
        "first name": "Олена",
        "last name": "Лисенко",
        photo: "./img/trainers/trainer-f3.jpg",
        specialization: "Басейн",
        category: "спеціаліст",
        experience: "4 роки",
        description:
            "Олена спеціалізується на синхронному плаванні. Вона тренує команди різного рівня. Її команди завжди займають призові місця на змаганнях.",
    },
    {
        "first name": "Андрій",
        "last name": "Волков",
        photo: "./img/trainers/trainer-m4.jpg",
        specialization: "Бійцівський клуб",
        category: "інструктор",
        experience: "1 рік",
        description:
            "Андрій має досвід у вивченні різних бойових мистецтв. Він викладає техніку та тактику бою. Його учні здобувають перемоги на міжнародних турнірах.",
    },
    {
        "first name": "Наталія",
        "last name": "Романенко",
        photo: "./img/trainers/trainer-f4.jpg",
        specialization: "Дитячий клуб",
        category: "спеціаліст",
        experience: "3 роки",
        description:
            "Наталія розробила унікальну програму для дітей дошкільного віку. Вона допомагає дітям розвивати фізичні та ментальні навички. Її класи завжди веселі та динамічні.",
    },
    {
        "first name": "Віталій",
        "last name": "Козлов",
        photo: "./img/trainers/trainer-m5.jpg",
        specialization: "Тренажерний зал",
        category: "майстер",
        experience: "10 років",
        description:
            "Віталій спеціалізується на функціональному тренуванні. Він розробив ряд ефективних тренувальних програм. Його клієнти швидко досягають бажаних результатів.",
    },
    {
        "first name": "Юлія",
        "last name": "Кравченко",
        photo: "./img/trainers/trainer-f5.jpg",
        specialization: "Басейн",
        category: "спеціаліст",
        experience: "4 роки",
        description:
            "Юлія є експертом у водних видах спорту. Вона проводить тренування з аквагімнастики та аеробіки. Її учні демонструють вражаючі показники на змаганнях.",
    },
    {
        "first name": "Олег",
        "last name": "Мельник",
        photo: "./img/trainers/trainer-m6.jpg",
        specialization: "Бійцівський клуб",
        category: "майстер",
        experience: "12 років",
        description:
            "Олег є визнаним майстром в бойових мистецтвах. Він тренує чемпіонів різних вагових категорій. Його методики вважаються одними з найефективніших у світі бойових мистецтв.",
    },
    {
        "first name": "Лідія",
        "last name": "Попова",
        photo: "./img/trainers/trainer-f6.jpg",
        specialization: "Дитячий клуб",
        category: "інструктор",
        experience: "1 рік",
        description:
            "Лідія має великий досвід у роботі з дітьми. Вона організовує різноманітні спортивні ігри та заняття. Її класи завжди допомагають дітям розвивати соціальні навички та командний дух.",
    },
    {
        "first name": "Роман",
        "last name": "Семенов",
        photo: "./img/trainers/trainer-m7.jpg",
        specialization: "Тренажерний зал",
        category: "спеціаліст",
        experience: "2 роки",
        description:
            "Роман є експертом у кросфіту та функціональних тренуваннях. Він розробив власні програми для різних вікових груп. Його учні часто отримують нагороди на фітнес-турнірах.",
    },
    {
        "first name": "Анастасія",
        "last name": "Гончарова",
        photo: "./img/trainers/trainer-f7.jpg",
        specialization: "Басейн",
        category: "інструктор",
        experience: "1 рік",
        description:
            "Анастасія фокусується на водних програмах для здоров'я та фітнесу. Вона проводить тренування для осіб з різним рівнем підготовки. Її учні відзначають покращення здоров'я та благополуччя після занять.",
    },
    {
        "first name": "Валентин",
        "last name": "Ткаченко",
        photo: "./img/trainers/trainer-m8.jpg",
        specialization: "Бійцівський клуб",
        category: "спеціаліст",
        experience: "2 роки",
        description:
            "Валентин є експертом в таеквондо та кікбоксингу. Він викладає техніку, тактику та стратегію бою. Його учні часто стають чемпіонами на національних та міжнародних змаганнях.",
    },
    {
        "first name": "Лариса",
        "last name": "Петренко",
        photo: "./img/trainers/trainer-f8.jpg",
        specialization: "Дитячий клуб",
        category: "майстер",
        experience: "7 років",
        description:
            "Лариса розробила комплексну програму для розвитку фізичних та інтелектуальних навичок дітей. Вона проводить заняття в ігровій формі. Її методика допомагає дітям стати активними та розумними.",
    },
    {
        "first name": "Олексій",
        "last name": "Петров",
        photo: "./img/trainers/trainer-m9.jpg",
        specialization: "Басейн",
        category: "майстер",
        experience: "11 років",
        description:
            "Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
    },
    {
        "first name": "Марина",
        "last name": "Іванова",
        photo: "./img/trainers/trainer-f9.jpg",
        specialization: "Тренажерний зал",
        category: "спеціаліст",
        experience: "2 роки",
        description:
            "Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
    },
    {
        "first name": "Ігор",
        "last name": "Сидоренко",
        photo: "./img/trainers/trainer-m10.jpg",
        specialization: "Дитячий клуб",
        category: "інструктор",
        experience: "1 рік",
        description:
            "Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
    },
    {
        "first name": "Наталія",
        "last name": "Бондаренко",
        photo: "./img/trainers/trainer-f10.jpg",
        specialization: "Бійцівський клуб",
        category: "майстер",
        experience: "8 років",
        description:
            "Наталія є майстром у бойових мистецтвах. Вона вивчала різні техніки та стили, із якими працює зі своїми учнями. Її підхід до навчання відповідає найвищим стандартам.",
    },
    {
        "first name": "Андрій",
        "last name": "Семенов",
        photo: "./img/trainers/trainer-m11.jpg",
        specialization: "Тренажерний зал",
        category: "інструктор",
        experience: "1 рік",
        description:
            "Андрій спеціалізується на функціональному тренуванні. Він розробив власну систему вправ для зміцнення корпусу. Його учні завжди отримують видимі результати.",
    },
    {
        "first name": "Софія",
        "last name": "Мельник",
        photo: "./img/trainers/trainer-f11.jpg",
        specialization: "Басейн",
        category: "спеціаліст",
        experience: "6 років",
        description:
            "Софія працює з аквагімнастикою. Вона вивчила різні техніки та стили плавання. Її заняття допомагають клієнтам розслабитися та покращити фізичну форму.",
    },
    {
        "first name": "Дмитро",
        "last name": "Ковальчук",
        photo: "./img/trainers/trainer-m12.png",
        specialization: "Дитячий клуб",
        category: "майстер",
        experience: "10 років",
        description:
            "Дмитро спеціалізується на розвитку дитячого спорту. Він розробив унікальну програму для малюків. Його методики забезпечують гармонійний розвиток дітей.",
    },
    {
        "first name": "Олена",
        "last name": "Ткаченко",
        photo: "./img/trainers/trainer-f12.jpg",
        specialization: "Бійцівський клуб",
        category: "спеціаліст",
        experience: "5 років",
        description:
            "Олена є відомим тренером у жіночому бойовому клубі. Вона вивчила різні техніки самооборони. Її підхід дозволяє її ученицям відчувати себе впевнено в будь-яких ситуаціях.",
    },
];

//УСІ ГЛОБАЛЬНІ ЗМІННІ ДЛЯ ПОШУКУ ЕЛЕМЕНТІВ ==================
const trainerCardContainer = document.querySelector(".trainers-cards__container");
const trainerCardTemplate = document.querySelector("#trainer-card").content;
const sortedForm = document.querySelector(".sorting");
const filterForm = document.querySelector(".sidebar");
const sortedButtons = document.querySelectorAll(".sorting__btn");
const modalTemplateElement = document.querySelector("#modal-template").content;
const showMoreButton = document.querySelector(".trainer__show-more");
const filterSubmit = document.querySelector(".filters__submit");

//НОВИЙ МАСИВ "МАР" ДЛЯ ЗБЕРІГАННЯ АЙДІ-ШОК КАРТОК ДЛЯ ВІКРИТТЯ МОДАЛЬНОГО ВІКНА ЗІ ЗБЕРЕЖЕННЯМ ПОРЯДКУ ЗНАЧЕННЬ
const idToCardMap = new Map();

//НОВИЙ МАСИВ ДЛЯ ЗБЕРІГАННЯ БАЗОВОГО + ДОДАНО АЙДІ ДЛЯ КОЖНОГО ЗАПИСУ
const dataWithId = DATA.map((item, index) => ({ id: index, ...item }));

//ІНДЕКС ДЛЯ УНІКАЛЬНИХ ЕЛЕМЕНТІВ НА КНОПКАХ ФІЛЬТРАЦІЇ
let index = 0;

//МАСИВ, ЯКИЙ ЗБЕРІГАЄ НА ПОЧАТКУ У СОБІ ЗНАЧЕННЯ МАСИВУ АЛЕ З АЙДІ-ШНИКАМИ
let filteredData = dataWithId;

//ПОРОЖНІЙ МАСИВ ДЛЯ ЗБЕРІГАННЯ РЕЗУЛЬТАТУ СОРТУВАННЯ
const sortedResults = {};

//ЗМІННА, ЯКА ЗБЕРІГАЄ ЗНАЧЕННЯ ДЛЯ СОРТУВАННЯ, ЩОБ ЯКЩО КНОПКА "ПОКАЗАТИ" НА ФІЛЬТРАЦІЇ НАТИСНУТА ХОЧА Б
//ОДИН РАЗ, СЛІД ВИКОНУВАТИ СОРТУВАННЯ ВІДФІЛЬТРОВАНИХ ДАНИХ, ІНАКШЕ ДЕФОЛТНЕ ЗНАЧЕННЯ
let showButtonClicked = 0;

//ЗМІННІ, ЯКІ ЗБЕРІГАЮТЬ ЗНАЧЕННЯ СОРТУВАННЯ(ЯКА З КНОПОК СОРТУВАННЯ БУЛА НАТИСНУТА)
//ТА ДВОХ ФІЛЬТРІВ: "ЗА НАПРЯМОМ" ТА "ЗА КАТЕГОРІЄЮ"
let selectedDirection;
let selectedCategory;
let sortingIndex;

//ВСІ ЛІСТЕНЕРИ ДЛЯ ВИКОНАННЯ ЗАВДАННЬ==================

//СТВОРЕННЯ ІНДЕКСУ ДЛЯ КНОПОК СОРТУВАННЯ==================
sortedButtons.forEach((button) => {
    button.setAttribute("data-index", index);
    index++;
    button.classList.remove("sorting__btn--active");
    sortedButtons[0].classList.add("sorting__btn--active");
})

//ЛІСТЕНЕР ДЛЯ ЗАВАНТАЖЕННЯ СТОРІНКИ
document.addEventListener("DOMContentLoaded", (event) => {
    const header = document.querySelector(".page-header");
    const preloaderHTML = '<div id="preloader"><img src="fitness_preloader.gif" alt="Loading..."></div>';
    header.insertAdjacentHTML('afterend', preloaderHTML);
    const preloaderElement = document.querySelector("#preloader");

    setTimeout(function () {
        preloaderElement.style.display = 'none';
        renderAllCards(filteredData);
        sortedForm.removeAttribute("hidden");
        filterForm.removeAttribute("hidden");

        //ПЕРЕВІРЯЄМО ЧИ БУЛО ВЖЕ ОЧИЩЕННЯ localStorage, ЯКЩО НІ - ОЧИЩУЄМО ТА СТАВИМО isCleared
        if (!localStorage.getItem('isCleared')) {
            localStorage.clear();
            localStorage.setItem('isCleared', 'true');
        }
        const savedFilterDirection = localStorage.getItem('filterDirection');
        const savedFilterCategory = localStorage.getItem('filterCategory');
        const savedSortIndex = +localStorage.getItem('sortIndex');

        if (savedFilterDirection && savedFilterCategory) {
            //ЯКЩО Є ЗБЕРЕЖЕНІ ПАРАМЕТРИ ФІЛЬТРІВ - ЗАСТОСОВУЄМО
            document.querySelector(`input[name="direction"][value="${savedFilterDirection}"]`).checked = true;
            document.querySelector(`input[name="category"][value="${savedFilterCategory}"]`).checked = true;
            const filterButton = document.querySelector(".filters__submit");
            filterButton.click();
        }

        if (savedSortIndex) {
            //ЯКЩО Є ЗБЕРЕЖЕНІ ПАРАМЕТРИ СОРТУВАННЯ - ЗАСТОСОВУЄМО
            const sortingButton = document.querySelector(`.sorting__btn[data-index="${savedSortIndex}"]`);
            sortingButton.classList.add("sorting__btn--active");
            sortingButton.click();
        }
    }, 3500);
});

//ЛІСТЕНЕР ДЛЯ ВІДКРИТТЯ МОДАЛЬНОГО ВІКНА З ІНФОРМАЦІЄЮ ПРО ТРЕНЕРА
trainerCardContainer.addEventListener("click", (event) => {
    if (event.target.classList.contains('trainer__show-more')) {

        const modalWindow = modalTemplateElement.cloneNode(true);
        const cardElement = event.target.closest('.trainer');
        const cardID = +cardElement.dataset.id;

        const result = findTrainerById(cardID);

        modalWindow.querySelector(".modal__img").setAttribute("src", result.photo);
        modalWindow.querySelector(".modal__name").innerText = `${result['first name']} ${result["last name"]}`;
        modalWindow.querySelector(".modal__point--category").innerText = `Категорія: ${result.category}`;
        modalWindow.querySelector(".modal__point--experience").innerText = `Досвід: ${result.experience}`;
        modalWindow.querySelector(".modal__point--specialization").innerText = `Напрям тренера: ${result.specialization}`;
        modalWindow.querySelector(".modal__text").innerText = result.description;

        document.body.append(modalWindow);

        const modalDivElement = document.querySelector('.modal');

        modalDivElement.addEventListener('click', (event) => {
            if (event.target.closest('button') || event.target === modalDivElement) {
                modalDivElement.remove();
            }
        });
    }
});

//ЛІСТЕНЕР ДЛЯ СОРТУВАННЯ ПО НАТИСКАННЮ НА КНОПКИ СОРТУВАННЯ "ЗА ЗАМОВЧУВАННЯМ", "ЗА ПРІЗВИЩЕМ" ТА "ЗА ДОСВІДОМ"
sortedForm.addEventListener("click", (event) => {
    const target = event.target;
    const dataSortValue = target.dataset.index;
    sortingIndex = +dataSortValue;

    if (!target.classList.contains("sorting__btn")) {
        return;
    }
    if (showButtonClicked > 0) {
        //ЯКЩО КНОПКА НА ФІЛЬТРАХ НАТИСНУТА - СОРТУВАННЯ ФІЛЬТРОВАНИХ ДАНИХ
        updateSortedResults(filteredData);
    } else {
        //ЯКЩО КНОПКА НА ФІЛЬТРАХ НЕ НАТИСНУТА - СОРТУВАННЯ БАЗОВИХ ДАНИХ
        updateSortedResults(dataWithId);
    }

    let sortedResult = sortedResults[sortingIndex];

    sortedButtons.forEach(button => {
        button.classList.remove("sorting__btn--active");
    });

    target.classList.add("sorting__btn--active");

    //РЕНДЕРИНГ ДАНИХ
    renderAllCards(sortedResult);
    updateIdToCardMap();

    localStorage.setItem('sortIndex', sortingIndex);
});

//ЛІСТЕНЕР ДЛЯ ФІЛЬТРАЦІЇ ПО НАТИСКАННЮ НА КНОПКУ "ПОКАЗАТИ" НА ФОРМІ ФІЛЬТРАЦІЇ
filterSubmit.addEventListener("click", (event) => {
    event.preventDefault();
    handleFilterChange(event);
    renderAllCards(filteredData);
    filterSubmit.classList.add("filters__submit--clicked");

    //ЗБІЛЬШУЄМО ЗМІННУ ДЛЯ ПЕРЕВІРКИ "ЧИ НАТИСНУТА КНОПКА "ПОКАЗАТИ" НА ФІЛЬТРАХ ХОЧА Б 1 РАЗ"
    showButtonClicked++;

    //ЗБЕРІГАЄМО ЗНАЧЕННЯ ФІЛЬТРАЦІЇ У localStorage
    localStorage.setItem('filterDirection', selectedDirection);
    localStorage.setItem('filterCategory', selectedCategory);
});

//ФУНКЦІЯ РЕНДЕРИНГУ ОДНІЄЇ КАРТКИ==================
function renderCard(trainer) {
    const cardRender = trainerCardTemplate.cloneNode(true);

    cardRender.querySelector(".trainer__img").setAttribute("src", trainer.photo);
    cardRender.querySelector(".trainer__name").innerText = `${trainer['first name']} ${trainer["last name"]}`;
    cardRender.querySelector(".trainer__show-more").innerText = "ПОКАЗАТИ";
    return cardRender;
}

//ФУНКЦІЯ РЕНДЕРИНГУ ВСИХ КАРТОК НА СТОРІНЦІ==================
function renderAllCards(data) {
    trainerCardContainer.innerHTML = '';

    data.forEach((trainer) => {
        const cardElement = renderCard(trainer);
        const cardID = trainer.id;
        cardElement.querySelector(".trainer").setAttribute("data-id", cardID);
        trainerCardContainer.appendChild(cardElement);
    });
    updateIdToCardMap();
}

//ФУНКЦІЯ, ЯКА ПРИЙМАЄ В СЕБЕ ДАННІ, ТА СВОРЮЄ ФІЛЬТРОВАНИЙ МАСИВ "filteredData"
function handleFilterChange(event) {
    selectedDirection = document.querySelector('input[name="direction"]:checked').value;
    selectedCategory = document.querySelector('input[name="category"]:checked').value;
    filteredData = filterData(selectedDirection, selectedCategory, dataWithId);
    updateIdToCardMap();
    updateSortedResults(filteredData);

    //ЦЯ ЧАСТИНА КОДУ РОБИТЬ КНОПКУ СОРТУВАННЯ "ЗА ЗАМОВЧУВАННЯМ" ДЕФОЛТНОЮ, ТОБТО ПРИ БУДЬ-ЯКІЙ ФІЛЬТРАЦІЇ БУДЕ ОБРАНО
    //СОРТУВАННЯ "ЗА ЗАМОВЧУВАННЯМ", АЛЕ ВОНО НЕ ПРАЦЮЄ З localStorage, ТОМУ ЗАКОММЕНТУВАВ
    /*sortedButtons.forEach(button => {
        button.classList.remove("sorting__btn--active");
    });
    sortedButtons[0].classList.add("sorting__btn--active");*/
}

//ФУНКЦІЯ, ЯКА ПРИЙМАЄ В СЕБЕ ВИКОНАННЯ ДВОХ ФУНКЦІЙ: "filterByDirection" ТА "filterByCategory"
function filterData(direction, category, data) {
    return data.filter((elem) => {
        const directionValue = direction.toLowerCase();
        const categoryValue = category.toLowerCase();

        const directionInData = elem.specialization.toLowerCase();
        const categoryInData = elem.category.toLowerCase();

        return (
            (directionValue === 'all' || filterByDirection(directionValue, directionInData)) &&
            (categoryValue === 'all' || filterByCategory(categoryValue, categoryInData))
        );
    });
}

//ФУНКЦІЯ, ЯКА ПОВЕРТАЄ ЗНАЧЕННЯ ФІЛЬТРУ "ЗА НАПРЯМОМ"
function filterByDirection(directionValue, directionInData) {
    const directionMap = {
        'all': true,
        'gym': 'тренажерний зал',
        'fight club': 'бійцівський клуб',
        'kids club': 'дитячий клуб',
        'swimming pool': 'басейн'
    };

    return directionMap[directionValue] === directionInData;
}

//ФУНКЦІЯ, ЯКА ПОВЕРТАЄ ЗНАЧЕННЯ ФІЛЬТРУ "ЗА КАТЕГОРІЄЮ"
function filterByCategory(categoryValue, categoryInData) {
    const categoryMap = {
        'all': true,
        'master': 'майстер',
        'specialist': 'спеціаліст',
        'instructor': 'інструктор'
    };

    return categoryMap[categoryValue] === categoryInData;
}

//ФУНКЦІЯ СОРТУВАННЯ ДАНИХ, ПОВЕРТАЄ СОРТОВАНИЙ МАСИВ
const sortedData = (value, data) => {
    const sortFunctions = {
        "default": data => data.slice(),
        "sortedButtonSurname": data => data.slice().sort((a, b) => a['last name'].localeCompare(b['last name'])),
        "sortedButtonExperience": data => data.slice().sort((a, b) => parseInt(b.experience) - parseInt(a.experience))
    };

    const sortedResult = sortFunctions[value](data);
    updateIdToCardMap();
    return sortedResult;
};

//ФУНКЦІЯ, ЯКА ФІКСУЄ ЗМІНУ ДАНИХ ПРИ СОРТУВАННІ, ЯКЩО ДАНІ БУЛИ ВІДФІЛЬТРОВАНІ
//ЯКЩО НІ, ПОВЕРТАЄ СОРТОВАНІ МАСИВИ ТАКОЖ З МАСИВОМ З ІНДЕКСАМИ
function updateSortedResults(data) {
    sortedResults["0"] = sortedData("default", data);
    sortedResults["1"] = sortedData("sortedButtonSurname", data);
    sortedResults["2"] = sortedData("sortedButtonExperience", data);
}

//ФУНКЦІЯ ПОШУКУ ТРЕНЕРА ПО АЙДІ, ВИКОРИСТОВУЄ ПОШУК САМЕ ТОГО ТРЕНЕРА, КАРТКУ З ЯКИМ АЙДІ МИ ВІДКРИЄМО
function findTrainerById(id) {
    return dataWithId.find(trainer => trainer.id === id);
}

//ФУНКЦІЯ ОНОВЛЕННЯ ПОРОЖНЬГО МАСИВА "МАР", ПОТРІБНА ДЛЯ ТОГО, ЩОБ ЗБЕРІГАТИ ВІДПОВІДНІСТЬ МІЖ АЙДІ КАРТОК
//ТА ЇХ ОБ'ЄКТАМИ
function updateIdToCardMap() {

    //СПОЧАТКУ ОЧИЩУЄМО КОНТЕЙНЕР АЙДІ-ШОК
    idToCardMap.clear();

    trainerCardContainer.querySelectorAll('.trainer').forEach((card) => {
        const cardID = card.dataset.id;
        idToCardMap.set(cardID, card);
    });
}