import React from 'react';
import PropTypes from 'prop-types';
import FavoriteIcon from '../../component/svg/FavoriteIcon/FavoriteIcon.jsx';
import './FavoritePage.scss';

const FavoritePage = ({ favorites, handleFavoriteClick, products }) => {
    const renderFavoriteProducts = () => {
        if (!Array.isArray(favorites) || favorites.length === 0) {
            return <div className='emptyBlock'>
                <h2 className='empty'>You haven't added any items to the favorite yet</h2>
            </div>;
        }

        return favorites.map((favorite) => {
            const product = products.find((p) => p.id === favorite.id);
            if (!product) return null;

            return (
                <div key={product.id} className="favorite-item">
                    <div className="product-image-container">
                        <img src={product.image_path} alt={product.name} className="product-image" />
                    </div>
                    <div className="product-details">
                        <h3>{product.name}</h3>
                        <p>Цена: {product.price} грн</p>
                        <p>Артикул: {product.article}</p>
                    </div>
                    <FavoriteIcon
                        className="favorite-btn"
                        isFavorite={true}
                        onClick={() => handleFavoriteClick(product.id)}
                    />
                </div>
            );
        });
    };

    return (
        <div className="favorite-page">
            <h1>Favorite page</h1>
            <div className="favorite-grid">
                {renderFavoriteProducts()}
            </div>
        </div>
    );
};

FavoritePage.propTypes = {
    favorites: PropTypes.array.isRequired,
    handleFavoriteClick: PropTypes.func.isRequired,
    products: PropTypes.array,
};

export default FavoritePage;
