import React, { useState, useEffect } from 'react'; 
import PropTypes from 'prop-types'; 
import './CartPage.scss'; 
import RenderModalDelete from '../../component/RenderModalDelete/RenderModalDelete.jsx'; 

const CartPage = ({ products, cart, handleRemoveFromCart, handleAddToCart  }) => {
  const [itemCounts, setItemCounts] = useState({}); 
  const [showModalDelete, setShowModalDelete] = useState(false);
  const [itemToRemove, setItemToRemove] = useState(null); 
  const [isAddingToCart, setIsAddingToCart] = useState(true);

  const handleDecrease = (itemId) => {
    if (itemCounts[itemId] > 1) {
      setItemCounts((prevCounts) => ({
        ...prevCounts,
        [itemId]: prevCounts[itemId] - 1,
      }));
    }
  };

  const handleIncrease = (itemId) => {
    setItemCounts((prevCounts) => ({
      ...prevCounts,
      [itemId]: (prevCounts[itemId] || 0) + 1,
    }));
  };


  const getTotalItemCountInCart = (itemId) => {
    return itemCounts[itemId] || 0;
  };


  const cartItems = products.filter((product) => cart.includes(product.id));

  const [totalPrice, setTotalPrice] = useState(0); 


  useEffect(() => {
    const newTotalPrice = cartItems.reduce((acc, item) => {
      const itemCount = itemCounts[item.id] || 1; 
      return acc + item.price * itemCount; 
    }, 0);
    setTotalPrice(newTotalPrice); 
  }, [cartItems, itemCounts]); 

  useEffect(() => {
    const newCounts = {};
    cart.forEach((itemId) => {
      newCounts[itemId] = (newCounts[itemId] || 0) + 1; 
    });
    setItemCounts(newCounts); 
  }, [cart]);

  const handleRemove = (itemId) => {
    setItemToRemove(itemId);
    setShowModalDelete(true);
    setIsAddingToCart(false); 
  };

  if (cartItems.length === 0) {
    return (
      <div className="emptyCart">
        <h1>Cart Page</h1>
        <h2>You haven't added any items to the cart yet</h2>
      </div>
    );
  }

  return (
    <>
      <h1>Cart Page</h1>
      <div className="cart-page">
        <div className="cart-items">
          {cartItems.map((item) => (
            <div key={item.id} className="cart-item">
              <div className="item-image-container">
                <img src={item.image_path} alt={item.name} className="item-image" />
              </div>
              <div className="item-details">
                <h2 className="product-name">{item.name}</h2>
                <p className="product-storage">Storage: {item.storage} GB</p>
                <p className="product-price">Price: {item.price * getTotalItemCountInCart(item.id)} грн</p>
                <div className="item-counters">
                  <button className="counter-button" onClick={() => handleDecrease(item.id)}>-</button>
                  <span className="counter">{getTotalItemCountInCart(item.id)}</span>
                  <button className="counter-button" onClick={() => handleIncrease(item.id)}>+</button>
                </div>
                <button className="remove-button" onClick={() => handleRemove(item.id)}>X</button>
              </div>
            </div>
          ))}
        </div>
        <div className="total-price-block">
          <span>
            <h1>Total Price</h1>
          </span>
          <span>
            <span className="total-value">{totalPrice}</span> грн
          </span>
          <button className="buy-now-button">BUY NOW</button>
        </div>
      </div>
      {showModalDelete && (
        <RenderModalDelete
          setShowModalDelete={setShowModalDelete}
          handleAddToCart={handleAddToCart}
          src={cartItems.find((item) => item.id === itemToRemove)?.image_path}
          product={cartItems.find((item) => item.id === itemToRemove)}
          isAddingToCart={isAddingToCart}
          handleRemoveFromCart={handleRemoveFromCart} 
        />
      )}
    </>
  );
};


CartPage.propTypes = {
  products: PropTypes.array.isRequired, 
  cart: PropTypes.array.isRequired, 
  handleRemoveFromCart: PropTypes.func.isRequired, 
  handleAddToCart: PropTypes.func.isRequired, 
};

export default CartPage;