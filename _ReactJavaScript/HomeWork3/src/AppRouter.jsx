import React, { useState, useEffect } from 'react';
import { Routes, Route } from 'react-router-dom';
import axios from 'axios';
import ShopPage from './routes/ShopPage/ShopPage.jsx';
import HomePage from './routes/HomePage/HomePage.jsx';
import FavoritePage from './routes/FavoritePage/FavoritePage.jsx'; 
import CartPage from './routes/CartPage/CartPage.jsx'; 
import PropTypes from 'prop-types';
import Header from './component/Header/Header';

const AppRouter = () => {
    const [favorites, setFavorites] = useState(() => {
        const savedFavorites = localStorage.getItem('favorites');
        return savedFavorites ? JSON.parse(savedFavorites) : [];
    });

    const [cart, setCart] = useState(() => {
        const savedCart = localStorage.getItem('cart');
        return savedCart ? JSON.parse(savedCart) : [];
    });

    const [products, setProducts] = useState([]);

    const handleFavoriteClick = (productId) => {
        const updatedFavorites = Array.isArray(favorites)
            ? (favorites.includes(productId)
                ? favorites.filter(id => id !== productId)
                : [...favorites, productId])
            : [productId];
    
        setFavorites(updatedFavorites);
        localStorage.setItem('favorites', JSON.stringify(updatedFavorites));
    };

    const handleCartClick = (productId) => {
        const updatedCart = [...cart, productId];
        setCart(updatedCart);
        localStorage.setItem('cart', JSON.stringify(updatedCart));
    };

    const handleRemoveFromCart = (productId) => {
        const updatedCart = cart.filter((id) => id !== productId);
        setCart(updatedCart);
        localStorage.setItem('cart', JSON.stringify(updatedCart));
    };

    const handleAddToCart = (product) => {
        onCartClick(product.id);
        setIsModalDeleteOpen(false);
      };

    useEffect(() => {
        localStorage.setItem('favorites', JSON.stringify(favorites));
    }, [favorites]);

    useEffect(() => {
        localStorage.setItem('cart', JSON.stringify(cart));
    }, [cart]);

    useEffect(() => {
        axios.get('./public/database.json')
            .then(response => {
                setProducts(response.data);
            })
            .catch(error => {
                console.error('Error fetching products:', error);
            });
    }, []);

    const favoriteProducts = Array.isArray(products) ? products.filter(product => favorites.includes(product.id)) : [];

    return (
        <>
            <Header
                favorites={favorites}
                handleFavoriteClick={handleFavoriteClick}
                cart={cart}
                handleCartClick={handleCartClick}
            />
            <Routes>
                <Route path='/' element={<HomePage />} />
                <Route path='/shop' element={
                    <ShopPage
                        onFavoriteClick={handleFavoriteClick}
                        favorites={favorites}
                        cart={cart}
                        onCartClick={handleCartClick}
                        products={products}
                    />} />
                <Route path='/favorite' element={
                    <FavoritePage
                        favorites={favoriteProducts}
                        handleFavoriteClick={handleFavoriteClick}
                        products={products}
                    />} />
                <Route path='/cart' element={
                    <CartPage
                        products={products}
                        handleFavoriteClick={handleFavoriteClick}
                        cart={cart}
                        handleRemoveFromCart={handleRemoveFromCart}
                        handleAddToCart={handleAddToCart}
                    />} />
            </Routes>
        </>
    );
};

AppRouter.propTypes = {
    favorites: PropTypes.array.isRequired,
    cart: PropTypes.array.isRequired,
    handleFavoriteClick: PropTypes.func.isRequired,
    handleCartClick: PropTypes.func.isRequired,
};

export default AppRouter;