import React from 'react';
import ModalWrapper from '../ModalWrapper/ModalWrapper.jsx';
import Modal from '../Modal/Modal.jsx';
import ModalHeader from '../ModalHeader/ModalHeader.jsx';
import ModalFooter from '../ModalFooter/ModalFooter.jsx';
import ModalClose from '../ModalClose/ModalClose.jsx';
import ModalBody from '../ModalBody/ModalBody.jsx';
import ModalText from '../ModalText/ModalText.jsx';
import ModalImage from '../ModalImage/ModalImage.jsx';

import './RenderModalDelete.scss';

const RenderModalDelete = ({ setShowModalDelete, src, product, isAddingToCart, onCartClick, handleRemoveFromCart }) => {
  const handleConfirm = () => {
    if (isAddingToCart) {
      onCartClick(product.id);
    } else {
      handleRemoveFromCart(product.id); 
    }
    setShowModalDelete(false); 
  };

  const handleCancel = () => {
    setShowModalDelete(false);
  };

  return (
    <ModalWrapper onClick={() => setShowModalDelete(false)}>
      <Modal onClose={() => setShowModalDelete(false)} title="Confirmation">
        <ModalHeader>
          <ModalClose className="closeButton" onClick={() => setShowModalDelete(false)} />
        </ModalHeader>
        <ModalBody>
          <div className="modal-image-container">
            <ModalImage src={src} alt="Product Image" className="img" />
          </div>
          <h1>{isAddingToCart ? 'Add' : 'Delete'} {product.name} {isAddingToCart ? 'to' : 'from'} cart?</h1>
          <br></br>
          <ModalText text={`You are about to ${isAddingToCart ? 'add' : 'delete'} ${product.name} from cart`} />
        </ModalBody>
        <ModalFooter>
          <button onClick={handleCancel} className="secondaryButton">
            NO, CANCEL
          </button>
          <button onClick={handleConfirm} className="primaryButton">
            YES, {isAddingToCart ? 'Add' : 'Delete'}
          </button>
        </ModalFooter>
      </Modal>
    </ModalWrapper>
  );
};

export default RenderModalDelete;