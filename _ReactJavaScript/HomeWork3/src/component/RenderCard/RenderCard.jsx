import React, { useState } from 'react';
import './RenderCard.scss';

import RenderModalDelete from '../RenderModalDelete/RenderModalDelete.jsx';
import FavoriteIcon from '../svg/FavoriteIcon/FavoriteIcon.jsx';
import RenderModalFavorite from '../RenderModalFavorite/RenderModalFavorite.jsx';

const RenderCard = ({ products, onFavoriteClick, favorites, cart, onCartClick, handleRemoveFromCart }) => {
  const [isModalDeleteOpen, setIsModalDeleteOpen] = useState(false);
  const [isModalFavoriteOpen, setIsModalFavoriteOpen] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState(null);
  const [addToCartConfirmed, setAddToCartConfirmed] = useState(false);
  const [addedToCart, setAddedToCart] = useState({}); // Добавляем стейт для отслеживания добавления товара в корзину

  const toggleModalDelete = (product) => {
    setSelectedProduct(product);
    setIsModalDeleteOpen(true);
  };

  const toggleModalFavorite = (product) => {
    setSelectedProduct(product);
    setIsModalFavoriteOpen(!isModalFavoriteOpen);
  };

  const handleAddToCart = (product) => {
    onCartClick(product.id);
    setAddedToCart((prevAddedToCart) => ({
      ...prevAddedToCart,
      [product.id]: true, // Устанавливаем флаг, что товар был добавлен в корзину
    }));
    setIsModalDeleteOpen(false);
  };

  if (!Array.isArray(favorites)) {
    favorites = [favorites];
  }

  return (
    <div>
      {products.map((product) => (
        <div key={product.id} className="card">
          <div className="imageContainer">
            <img src={product.image_path} alt={product.name} className="image" />
            <FavoriteIcon
              className="cardIcon"
              isFavorite={favorites.includes(product.id)}
              onClick={() => {
                onFavoriteClick(product.id);
                toggleModalFavorite(product);
              }}
            />
          </div>
          <div className="cardInfo">
            <h2>{product.name}</h2>
            <div className="cardDescriptionContainer">
              <div className="cardDescription">
                <p>Storage: {product.storage}</p>
                <p>Color: {product.color}</p>
                <p>Price: {product.price} грн</p>
              </div>
              <div className="cardButtonContainer">
                <p className="cardArticle">Article: {product.article}</p>
                <button
                  className="cardButton secondaryButton"
                  onClick={() => {
                    toggleModalDelete(product);
                    setAddToCartConfirmed(true);
                  }}
                  disabled={addedToCart[product.id]} // Отключаем кнопку, если товар уже добавлен в корзину
                >
                  {addedToCart[product.id] ? 'Added to cart' : 'Add to cart'}
                </button>
              </div>
            </div>
          </div>
        </div>
      ))}
      {isModalDeleteOpen && (
        <RenderModalDelete
          setShowModalDelete={setIsModalDeleteOpen}
          src={selectedProduct.image_path}
          product={selectedProduct}
          onCartClick={onCartClick}
          handleRemoveFromCart={handleRemoveFromCart}
          isAddingToCart={!cart.includes(selectedProduct.id)}
        />
      )}
      {isModalFavoriteOpen && (
        <RenderModalFavorite
          setShowModalFavorite={setIsModalFavoriteOpen}
          onFavoriteClick={onFavoriteClick}
          product={selectedProduct}
          favorites={favorites}
        />
      )}
    </div>
  );
};

export default RenderCard;