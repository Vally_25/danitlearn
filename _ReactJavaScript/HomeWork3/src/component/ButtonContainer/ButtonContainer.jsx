// ButtonContainer.jsx
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import styles from './ButtonContainer.module.scss';
import CartIcon from '../svg/CartSVG/CartIcon.jsx';
import FavoriteIcon from '../svg/FavoriteIcon/FavoriteIcon.jsx';
import { NavLink } from 'react-router-dom';

const ButtonContainer = ({ favorites, cart, handleIncrease, handleDecrease }) => {
  useEffect(() => {

  }, [favorites])

  useEffect(() => {

  }, [cart])

  // Функция для подсчета количества конкретного товара в корзине
  const getItemCountInCart = (itemId) => cart.filter(item => item === itemId).length;

  return (
    <div className={styles.buttonContainer}>

      < NavLink to="/favorite"
        className={({ isActive }) =>
          isActive ? `${styles.active}` : 'none'}>
        <button className={styles.favorite}>
          <FavoriteIcon isStatic />
          <span className={styles.favoriteCount}>{favorites.length}</span>
        </button>
      </NavLink >

      < NavLink to="/cart"
        className={({ isActive }) =>
          isActive ? `${styles.active}` : 'none'}>
        {/* Пробрасываем количество товара при нажатии на кнопку */}
        <button className={styles.cart} onClick={() => handleIncrease(1, getItemCountInCart(1))}> 
          <CartIcon isStatic />
          <span className={styles.cartCount}>{cart.length}</span>
        </button>
      </NavLink >
    </div>
  );
};

ButtonContainer.propTypes = {
  favorites: PropTypes.array.isRequired,
  cart: PropTypes.array.isRequired,
  handleIncrease: PropTypes.func.isRequired,
  handleDecrease: PropTypes.func.isRequired,
};

export default ButtonContainer;
