import React from 'react';
import PropTypes from 'prop-types';
import './Header.scss';
import Logo from '../Logo/Logo.jsx';
import NavMenu from '../NavMenu/NavMenu.jsx';
import SearchButton from '../SearchButton/SearchButton.jsx';
import ButtonContainer from '../ButtonContainer/ButtonContainer.jsx';

const Header = ({ favorites, cart }) => {

  return (
    <div className="header">
      <Logo />
      <NavMenu />
      <SearchButton />
      <ButtonContainer favorites={favorites} cart={cart}/>
    </div>
  );
};

Header.propTypes = {
  favorites: PropTypes.array.isRequired,
  cart: PropTypes.array.isRequired,
  handleFavoriteClick: PropTypes.func.isRequired,
};

export default Header;