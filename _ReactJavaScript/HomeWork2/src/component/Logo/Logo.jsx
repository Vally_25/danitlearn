import React from 'react';
import PropTypes from 'prop-types';
import './Logo.scss';

const srcLogo = '../../../public/Logo.jpg';

const Logo = () => {
  return (
    <div className="logo">
        <img src={srcLogo}></img>
    </div>
  );
};

export default Logo;