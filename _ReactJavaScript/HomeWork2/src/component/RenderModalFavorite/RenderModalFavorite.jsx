import React from 'react';
import ModalWrapper from '../ModalWrapper/ModalWrapper.jsx';
import Modal from '../Modal/Modal.jsx';
import ModalHeader from '../ModalHeader/ModalHeader.jsx';
import ModalFooter from '../ModalFooter/ModalFooter.jsx';
import ModalClose from '../ModalClose/ModalClose.jsx';
import ModalBody from '../ModalBody/ModalBody.jsx';
import ModalText from '../ModalText/ModalText.jsx';
import ModalImage from '../ModalImage/ModalImage.jsx';
import Button from '../Button/Button.jsx';

import './RenderModalFavorite.scss';

const RenderModalFavorite = ({ setShowModalFavorite, src, onCartClick, product, favorites }) => {
  
  const handleAddToCart = () => {
    onCartClick();
    setShowModalFavorite(false);
  };

  const handleCancel = () => {
    setShowModalFavorite(false);
  };

  const isFavorite = favorites.includes(product.id); // Check if the product is in favorites

  return (
    <ModalWrapper onClick={() => setShowModalFavorite(false)}>
      <Modal onClose={() => setShowModalFavorite(false)} title="Modal with Text">
        <ModalHeader>
          <ModalClose onClick={() => setShowModalFavorite(false)} />
        </ModalHeader>
        <ModalBody>
          <h1>
            Product '{product.name}' {isFavorite ? 'added to' : 'removed from'} favorite
          </h1>
          <ModalText text="You can close this window clicked by OK" />
        </ModalBody>
        <ModalFooter>
          <button onClick={() => setShowModalFavorite(false)} className="primaryButton">OK</button>
        </ModalFooter>
      </Modal>
    </ModalWrapper>
  );
};

RenderModalFavorite.defaultProps = {
  setShowModalFavorite: () => {},
  src: '',
  onCartClick: () => {},
  product: {},
  favorites: [],
};

export default RenderModalFavorite;
