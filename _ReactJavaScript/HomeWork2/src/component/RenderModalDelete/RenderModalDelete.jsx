import React from 'react';
import ModalWrapper from '../ModalWrapper/ModalWrapper.jsx';
import Modal from '../Modal/Modal.jsx';
import ModalHeader from '../ModalHeader/ModalHeader.jsx';
import ModalFooter from '../ModalFooter/ModalFooter.jsx';
import ModalClose from '../ModalClose/ModalClose.jsx';
import ModalBody from '../ModalBody/ModalBody.jsx';
import ModalText from '../ModalText/ModalText.jsx';
import ModalImage from '../ModalImage/ModalImage.jsx';
import Button from '../Button/Button.jsx';

import './RenderModalDelete.scss';

const RenderModalDelete = ({ setShowModalDelete, src, onCartClick, product }) => {
  const handleAddToCart = () => {
    onCartClick();
    setShowModalDelete(false);
  };

  const handleCancel = () => {
    setShowModalDelete(false);
  };

  return (
    <ModalWrapper onClick={() => setShowModalDelete(false)}>
      <Modal onClose={() => setShowModalDelete(false)} title="Modal with Image">
        <ModalHeader>
          <ModalClose className="closeButton" onClick={() => setShowModalDelete(false)} />
        </ModalHeader>
        <ModalBody>
          <div className="modal-image-container">
            <ModalImage src={src} alt="Modal Image" className="img"/>
          </div>
          <h1>{product.name} added on cart</h1>
          <br></br>
          <ModalText text="You can cancel added product on cart clicked by NO, CANCEL" />
        </ModalBody>
        <ModalFooter>
          <button onClick={handleCancel} className="secondaryButton">
            NO, CANCEL
          </button>
          <button onClick={handleAddToCart} className="primaryButton">
            YES, ADD
          </button>
        </ModalFooter>
      </Modal>
    </ModalWrapper>
  );
};

export default RenderModalDelete;
