import React, { useState, useEffect } from 'react';
import axios from 'axios';
import RenderCard from '../RenderCard/RenderCard.jsx';
import "./ShopPage.scss";

const ShopPage = ({ onFavoriteClick, favorites, cart, onCartClick, showModalDelete, toggleModalDelete  }) => {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get('/public/database.json');
        setProducts(response.data);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, []);

  return (
    <div className="cardsContainer">
      <h1>Shop Page</h1>
      <RenderCard 
        products={products} 
        onFavoriteClick={onFavoriteClick} 
        favorites={favorites}
        onCartClick={onCartClick} cart={cart}
      />
    </div>
  );
};

export default ShopPage;
