import React, { useState, useEffect } from 'react';
import Header from './component/Header/Header.jsx';
import ShopPage from './component/PageShop/ShopPage.jsx';
import Button from './component/Button/Button.jsx';
import './App.css';

const App = () => {
  const [favorites, setFavorites] = useState(() => {
    const savedFavorites = localStorage.getItem('favorites');
    return savedFavorites ? JSON.parse(savedFavorites) : [];
  });

  const [cart, setCart] = useState(() => {
    const savedCart = localStorage.getItem('cart');
    return savedCart ? JSON.parse(savedCart) : [];
  });

  const handleFavoriteClick = (productId) => {
    const updatedFavorites = favorites.includes(productId)
      ? favorites.filter(id => id !== productId)
      : [...favorites, productId];
    setFavorites(updatedFavorites);
    localStorage.setItem('favorites', JSON.stringify(updatedFavorites));
  };

  const handleCartClick = (productId) => {
    const updatedCart = [...cart, productId];
    setCart(updatedCart);
    localStorage.setItem('cart', JSON.stringify(updatedCart));
  };

  useEffect(() => {
    localStorage.setItem('favorites', JSON.stringify(favorites));
  }, [favorites]);

  useEffect(() => {
    localStorage.setItem('cart', JSON.stringify(cart));
  }, [cart]);

  return (
    <div className="app">
      <Header
        favorites={favorites} 
        handleFavoriteClick={handleFavoriteClick}
        cart={cart} handleCartClick={handleCartClick}
      />
      <ShopPage
        onFavoriteClick={handleFavoriteClick} 
        favorites={favorites}
        cart={cart} onCartClick={handleCartClick}
      />
    </div>
  );
};

export default App;
