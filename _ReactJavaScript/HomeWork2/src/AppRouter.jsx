import { Routes, Route } from 'react-router-dom'

const AppRouter = () => {

    return{
       <Routes>
        <Route path="/" element={<h1>Home</h1>} />
        <Route path="/shop" element={<h1>Shop</h1>} />
       </Routes>
    }
}

export default AppRouter;