import React, { useState } from 'react';
import Modal from './component/Modal/Modal.jsx';
import ModalWrapper from './component/ModalWrapper/ModalWrapper.jsx';
import ModalHeader from './component/ModalHeader/ModalHeader.jsx';
import ModalFooter from './component/ModalFooter/ModalFooter.jsx';
import ModalClose from './component/ModalClose/ModalClose.jsx';
import ModalBody from './component/ModalBody/ModalBody.jsx';
import ModalImage from './component/ModalImage/ModalImage.jsx';
import ModalText from './component/ModalText/ModalText.jsx';
import Button from './component/Button/Button.jsx';
import './App.css';

const App = () => {
  const [showModalImage, setShowModalImage] = useState(false);
  const [showModalText, setShowModalText] = useState(false);

  const toggleModalImage = () => setShowModalImage(!showModalImage);
  const toggleModalText = () => setShowModalText(!showModalText);

  return (
    <div className="app">

      <Button onClick={toggleModalImage}>Open Modal with Image</Button>
      <Button onClick={toggleModalText}>Open Modal with Text</Button>

      {showModalImage && (
        <ModalWrapper onClick={() => setShowModalImage(false)}>
          <Modal onClose={() => setShowModalImage(false)} title="Modal with Image">
            <ModalHeader>
              <ModalClose className="closeButton" onClick={() => setShowModalImage(false)} />
            </ModalHeader>
            <ModalBody>
              <div className="modal-image-container">
                <ModalImage src="path/to/image" alt="Modal Image" />
              </div>
              <h1>Product Delete!</h1>
              <br></br>
              <ModalText text="By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted." />
            </ModalBody>
            <ModalFooter>
              <button onClick={() => setShowModalImage(false)} className="primaryButton">NO, CANCEL</button>
              <button onClick={() => setShowModalImage(false)} className="secondaryButton">YES, DELETE</button>
            </ModalFooter>
          </Modal>
        </ModalWrapper>
      )}

      {showModalText && (
        <ModalWrapper onClick={() => setShowModalText(false)}>
          <Modal onClose={() => setShowModalText(false)} title="Modal with Text">
            <ModalHeader>
              <ModalClose onClick={() => setShowModalText(false)} />
            </ModalHeader>
            <ModalBody>
            <h1>Add Product “NAME”</h1>
              <ModalText text="Description for you product" />
            </ModalBody>
            <ModalFooter>
              <button onClick={() => setShowModalText(false)} className="primaryButton">ADD TO FAVORITES</button>
            </ModalFooter>
          </Modal>
        </ModalWrapper>
      )}
    </div>
  );
};

export default App;
