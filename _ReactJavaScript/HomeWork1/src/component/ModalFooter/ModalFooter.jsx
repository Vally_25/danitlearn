import React from 'react';
import PropTypes from 'prop-types';
import './ModalFooter.sass';

const ModalFooter = ({ children }) => (
  <div className="modal-footer">
    {children}
  </div>
);

ModalFooter.propTypes = {
  children: PropTypes.node.isRequired,
};

export default ModalFooter;