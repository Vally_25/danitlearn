import React, { useState } from 'react';
import './ModalWrapper.sass';

const ModalWrapper = ({ children, onClick }) => {
  const [isOpen, setIsOpen] = useState(true);

  const handleClose = () => {
    setIsOpen(false);
    onClick && onClick();
  };

  const handleWrapperClick = (e) => {
    if (e.target === e.currentTarget) {
      handleClose();
    }
  };

  return (
    <div className={`modal-wrapper ${isOpen ? 'visible' : 'hidden'}`} onClick={handleWrapperClick}>
      <div className="modal" onClick={(e) => e.stopPropagation()}>
        {children}
      </div>
    </div>
  );
};

export default ModalWrapper;