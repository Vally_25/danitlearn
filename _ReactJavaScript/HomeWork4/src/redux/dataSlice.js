import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

export const fetchProducts = createAsyncThunk(
  'data/fetchProducts',
  async () => {
    const response = await axios.get('./public/database.json');
    return response.data;
  }
);

const dataSlice = createSlice({
  name: 'data',
  initialState: {
    favorites: [],
    cart: [],
    products: [],
    isModalDeleteOpen: false,
    isModalFavoriteOpen: false,
    selectedProduct: [],
    isAddedToFavorites: false,
    selectedProducts: [],
  },
  reducers: {

    increaseItemCount: (state, action) => {
      const itemId = action.payload;
      const itemIndex = state.cart.findIndex(item => item.id === itemId);
      if (itemIndex !== -1) {
        state.cart[itemIndex].count += 1;
      }
    },
    decreaseItemCount: (state, action) => {
      const itemId = action.payload;
      const itemIndex = state.cart.findIndex(item => item.id === itemId);
      if (itemIndex !== -1 && state.cart[itemIndex].count > 1) {
        state.cart[itemIndex].count -= 1;
      }
    },

    addToFavorites: (state, action) => {
      const productId = action.payload;
      if (!state.favorites.includes(productId)) {
        state.favorites.push(productId);
      }
    },
    removeFromFavorites: (state, action) => {
      const productId = action.payload;
      state.favorites = state.favorites.filter(item => item !== productId);
      state.isAddedToFavorites = false;
      state.selectedProducts = state.selectedProducts.filter(item => item.id !== productId);
    },
    addToCart: (state, action) => {
      const productId = action.payload.id;
      const existingItemIndex = state.cart.findIndex(item => item.id === productId);

      if (existingItemIndex !== -1) {
        state.cart[existingItemIndex].count += 1;
      } else {
        state.cart.push({ ...action.payload, count: 1 });
      }
    },
    removeFromCart: (state, action) => {
      const productId = action.payload;
      state.cart = state.cart.filter(item => item.id !== productId);
    },
    handleCartClick: (product) => (dispatch, getState) => {
      const cart = getState().data.cart;
      const productId = product.id;
      const productInCart = cart.find(item => item.id === productId);
      if (productInCart) {
        dispatch(removeFromCart(productId));
      } else {
        dispatch(addToCart(product));
      }
    },
    openModalDelete: (state, action) => {
      state.isModalDeleteOpen = true;
      state.selectedProduct = action.payload;
    },
    closeModalDelete: (state) => {
      state.isModalDeleteOpen = false;
      state.selectedProduct = null;
    },
    openModalFavorite: (state, action) => {
      state.isModalFavoriteOpen = true;
      state.selectedProduct = action.payload.product;
      state.isAddedToFavorites = action.payload.isAdded;
    },
    closeModalFavorite: (state) => {
      state.isModalFavoriteOpen = false;
      state.selectedProduct = null;
      state.isAddedToFavorites = false;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchProducts.fulfilled, (state, action) => {
        state.products = action.payload;
      });
  },
});

export const {
  addToFavorites,
  removeFromFavorites,
  addToCart,
  removeFromCart,
  openModalDelete,
  closeModalDelete,
  openModalFavorite,
  closeModalFavorite,
  increaseItemCount,
  decreaseItemCount,
} = dataSlice.actions;

export const toggleAddToFavorites = (product) => (dispatch, getState) => {
  const favorites = getState().data.favorites;
  const productId = product.id;
  if (favorites.includes(productId)) {
    dispatch(removeFromFavorites(productId));
  } else {
    dispatch(addToFavorites(productId));
  }
};

export const handleCartClick = (product) => (dispatch, getState) => {
  const cart = getState().data.cart;
  const productId = product.id;
  if (cart.includes(productId)) {
    dispatch(removeFromCart(productId));
  } else {
    dispatch(addToCart(productId));
  }
};

export default dataSlice.reducer;
