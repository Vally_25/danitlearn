import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import styles from './NavMenu.module.scss';

const NavMenu = () => {
  return (
    <ul className={styles.NavMenu}>
      <NavLink to="/"
        className={({ isActive }) =>
          isActive ? `${styles.navLink} ${styles.active}` : styles.navLink}>
        <li><p>Home</p></li>
      </NavLink>
      <NavLink to="/shop"
        className={({ isActive }) =>
          isActive ? `${styles.navLink} ${styles.active}` : styles.navLink}>
        <li><p>Shop</p></li>
      </NavLink>
      <NavLink to="/contacts"
        className={({ isActive }) =>
          isActive ? `${styles.navLink} ${styles.active}` : styles.navLink}>
        <li><p>Contacts</p></li>
      </NavLink>
    </ul>
  );
};

export default NavMenu;
