// ButtonContainer.jsx

import React from 'react';
import PropTypes from 'prop-types';
import styles from './ButtonContainer.module.scss';
import CartIcon from '../svg/CartSVG/CartIcon.jsx';
import FavoriteIcon from '../svg/FavoriteIcon/FavoriteIcon.jsx';
import { NavLink } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { addToCart } from '../../redux/dataSlice';

const ButtonContainer = () => {
  const favorites = useSelector(state => state.data.favorites);
  const cart = useSelector(state => state.data.cart);
  const dispatch = useDispatch();

  const getItemCountInCart = (itemId) => cart.filter(item => item === itemId).length;
  const uniqueItemsCount = new Set(cart).size;

  return (
    <div className={styles['buttonContainer']}>

      <NavLink to="/favorite"
        className={({ isActive }) =>
          isActive ? `${styles['active']}` : 'none'}>
        <button className={styles['favorite']}>
          <FavoriteIcon isStatic />
          <span className={styles['favoriteCount']}>{favorites.length}</span>
        </button>
      </NavLink >

      <NavLink to="/cart"
        className={({ isActive }) =>
          isActive ? `${styles.active}` : 'none'}>
        <button className={styles['cart']}>
          <CartIcon isStatic />
          <span className={styles['cartCount']}>{uniqueItemsCount}</span>
        </button>
      </NavLink >
    </div>
  );
};

ButtonContainer.propTypes = {};

export default ButtonContainer;
