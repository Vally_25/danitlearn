import React from 'react';
import './RenderModalDelete.scss';
import ModalWrapper from '../ModalWrapper/ModalWrapper.jsx';
import Modal from '../Modal/Modal.jsx';
import ModalHeader from '../ModalHeader/ModalHeader.jsx';
import ModalFooter from '../ModalFooter/ModalFooter.jsx';
import ModalClose from '../ModalClose/ModalClose.jsx';
import ModalBody from '../ModalBody/ModalBody.jsx';
import ModalText from '../ModalText/ModalText.jsx';
import ModalImage from '../ModalImage/ModalImage.jsx';
import { useDispatch, useSelector } from 'react-redux';
import Button from '../Button/Button.jsx';

import { removeFromCart, addToCart } from '../../redux/dataSlice.js';

const RenderModalDelete = ({ onClose, product, isFromCartPage }) => {
  const dispatch = useDispatch();
  const cart = useSelector((state) => state.data.cart);

  const handleConfirmAction = () => {
    const isProductInCart = cart.some(item => item.id === product.id);
    if (isFromCartPage) {
      dispatch(removeFromCart(product.id));
    } else {
      dispatch(addToCart(product));
    }
    onClose();
  };

  const handleCancel = () => {
    onClose();
  };

  const titleText = isFromCartPage ? 'Remove' : 'Add';
  const actionText = isFromCartPage ? 'YES, REMOVE' : 'YES, ADD';

  return (
    <ModalWrapper onClick={onClose}>
      <Modal onClose={onClose} title="Confirmation">
        <ModalHeader>
          <ModalClose className="closeButton" onClick={onClose} />
        </ModalHeader>
        <ModalBody>
          <div className="modal-image-container">
            <ModalImage src={product.image_path} alt="Product Image" className="img" />
          </div>
          <h1>{titleText} {product.name} {isFromCartPage ? 'from cart?' : 'to cart?'}</h1>
          <ModalText text={`You are about to ${titleText.toLowerCase()} ${product.name} ${isFromCartPage ? 'from cart' : 'to cart'}`} />
        </ModalBody>
        <ModalFooter>
          <Button onClick={handleCancel} className="secondaryButton">
            NO, CANCEL
          </Button>
          <Button onClick={handleConfirmAction} className="primaryButton">
            {actionText}
          </Button>
        </ModalFooter>
      </Modal>
    </ModalWrapper>
  );
};

export default RenderModalDelete;
