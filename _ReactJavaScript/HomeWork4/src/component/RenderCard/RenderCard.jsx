import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { openModalFavorite, closeModalFavorite, addToFavorites, removeFromFavorites, openModalDelete, closeModalDelete } from '../../redux/dataSlice.js';
import FavoriteIcon from '../svg/FavoriteIcon/FavoriteIcon.jsx';
import RenderModalFavorite from '../RenderModalFavorite/RenderModalFavorite.jsx';
import RenderModalDelete from '../RenderModalDelete/RenderModalDelete.jsx';
import './RenderCard.scss';

const RenderCard = ({ products }) => {
  const dispatch = useDispatch();
  const favorites = useSelector((state) => state.data.favorites);
  const selectedProduct = useSelector((state) => state.data.selectedProduct);
  const isModalDeleteOpen = useSelector((state) => state.data.isModalDeleteOpen);
  const isModalFavoriteOpen = useSelector((state) => state.data.isModalFavoriteOpen);

  const handleFavoriteClick = (product) => {
    const isProductInFavorites = favorites.includes(product.id);
    if (isProductInFavorites) {
      dispatch(removeFromFavorites(product.id));
    } else {
      dispatch(addToFavorites(product.id));
    }
    dispatch(openModalFavorite({ product: product, isAdded: !isProductInFavorites }));
  };

  const handleCartClick = (product) => {
    dispatch(openModalDelete(product));
  };

  const handleCloseModal = () => {
    if (isModalDeleteOpen) {
      dispatch(closeModalDelete());
    } else if (isModalFavoriteOpen) {
      dispatch(closeModalFavorite());
    }
  };

  return (
    <div>
      {products.map((product) => (
        <div key={product.id} className="card">
          <div className="imageContainer">
            <img src={product.image_path} alt={product.name} className="image" />
            <FavoriteIcon
              className="cardIcon"
              isFavorite={favorites.includes(product.id)}
              onClick={() => handleFavoriteClick(product)}
            />
          </div>
          <div className="cardInfo">
            <h2>{product.name}</h2>
            <div className="cardDescriptionContainer">
              <div className="cardDescription">
                <p>Storage: {product.storage}</p>
                <p>Color: {product.color}</p>
                <p>Price: {product.price} грн</p>
              </div>
              <div className="cardButtonContainer">
                <p className="cardArticle">Article: {product.article}</p>
                <button
                  className="cardButton secondaryButton"
                  onClick={() => handleCartClick(product)}
                > Add to cart
                </button>
              </div>
            </div>
          </div>
        </div>
      ))}
      {isModalDeleteOpen && <RenderModalDelete onClose={handleCloseModal} product={selectedProduct} />}
      {isModalFavoriteOpen && <RenderModalFavorite onClose={handleCloseModal} selectedProduct={selectedProduct} />}
    </div>
  );
};

export default RenderCard;
