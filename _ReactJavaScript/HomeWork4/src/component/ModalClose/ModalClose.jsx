import React from 'react';
import PropTypes from 'prop-types';
import './ModalClose.scss'

const ModalClose = ({ onClick }) => (
  <button className="modal-close" onClick={onClick}>X</button>
);

ModalClose.propTypes = {
  onClick: PropTypes.func.isRequired,
};

export default ModalClose;