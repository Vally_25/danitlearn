import React from 'react';
import './RenderModalFavorite.scss';
import ModalWrapper from '../ModalWrapper/ModalWrapper.jsx';
import Modal from '../Modal/Modal.jsx';
import Button from '../Button/Button.jsx';
import ModalHeader from '../ModalHeader/ModalHeader.jsx';
import ModalFooter from '../ModalFooter/ModalFooter.jsx';
import ModalClose from '../ModalClose/ModalClose.jsx';
import ModalBody from '../ModalBody/ModalBody.jsx';
import { useSelector } from 'react-redux';

const RenderModalFavorite = ({ onClose }) => {
  const selectedProduct = useSelector((state) => state.data.selectedProduct);
  const isAddedToFavorites = useSelector((state) => state.data.isAddedToFavorites);

  const modalHeaderText = isAddedToFavorites
  ? `Product '${selectedProduct && selectedProduct.name ? selectedProduct.name : ''}' added to favorite`
  : `Product '${selectedProduct && selectedProduct.name ? selectedProduct.name : ''}' removed from favorite`;


  return (
    <ModalWrapper onClick={onClose}>
      <Modal onClose={onClose}>
        <ModalHeader>
          <ModalClose onClick={onClose} />
        </ModalHeader>
        <ModalBody>
          <h1>{modalHeaderText}</h1>
        </ModalBody>
        <ModalFooter>
          <Button onClick={onClose} className="primaryButton">
            OK
          </Button>
        </ModalFooter>
      </Modal>
    </ModalWrapper>
  );
};

export default RenderModalFavorite;
