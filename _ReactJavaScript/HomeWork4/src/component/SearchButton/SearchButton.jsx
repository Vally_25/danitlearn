import React from 'react';
import PropTypes from 'prop-types';
import './SearchButton.scss';


const SearchButton = ({ placeholder, onChange }) => {
  return (
    <div className="SearchButton">
          <i className="search-icon fas fa-search"></i>
        <input
          type="text"
          placeholder="Search"
          onChange={onChange}
          className="search-input"
        />
      </div>
  );
}

SearchButton.propTypes = {
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
};

export default SearchButton;