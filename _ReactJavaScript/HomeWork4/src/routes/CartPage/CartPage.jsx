import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import styles from './CartPage.module.scss';
import RenderModalDelete from '../../component/RenderModalDelete/RenderModalDelete.jsx';
import { useDispatch, useSelector } from 'react-redux';
import { removeFromCart, increaseItemCount, decreaseItemCount } from '../../redux/dataSlice.js';

const CartPage = ({ products }) => {
  const dispatch = useDispatch();
  const cart = useSelector((state) => state.data.cart);

  const [itemCounts, setItemCounts] = useState({});
  const [showModalDelete, setShowModalDelete] = useState(false);
  const [itemToRemove, setItemToRemove] = useState(null);

  const handleIncrease = (item) => {
    dispatch(increaseItemCount(item.id));
  };
  
  const handleDecrease = (item) => {
    if (getTotalItemCountInCart(item.id) > 1) {
      dispatch(decreaseItemCount(item.id));
    }
  };

  const getTotalItemCountInCart = (itemId) => {
    const item = cart.find(i => i.id === itemId);
    return item ? item.count : 0;
  };
  
  const cartIds = cart.map((product) => product.id);
  const cartItems = products.filter((product) => cartIds.includes(product.id));

  const [totalPrice, setTotalPrice] = useState(0);

  useEffect(() => {
    const newTotalPrice = cartItems.reduce((acc, item) => {
      const itemCount = getTotalItemCountInCart(item.id);
      return acc + item.price * itemCount;
    }, 0);
    setTotalPrice(newTotalPrice);
  }, [cartItems, itemCounts]);

  useEffect(() => {
    const newCounts = {};
    cart.forEach((item) => {
      newCounts[item.id] = 1;
    });
    setItemCounts(newCounts);
  }, [cart]);

  const handleRemove = (itemId) => {
    setItemToRemove(itemId);
    setShowModalDelete(true);
  };

  const handleCloseDeleteModal = () => {
    setShowModalDelete(false);
  };

  const handleConfirmDelete = () => {
    dispatch(removeFromCart(itemToRemove));
    handleCloseDeleteModal();
  };

  if (cartItems.length === 0) {
    return (
      <div className={styles["emptyCart"]}>
        <h1>Cart Page</h1>
        <h2>You haven't added any items to the cart yet</h2>
      </div>
    );
  }

  return (
    <>
      <h1>Cart Page</h1>
      <div className={styles['cart-page']}>
        <div className={styles['cart-items']}>
          {cartItems.map((item) => (
            <div key={item.id} className={styles['cart-item']}>
              <div className={styles['item-image-container']}>
                <img src={item.image_path} alt={item.name} className={styles['item-image']} />
              </div>
              <div className={styles['item-details']}>
                <h2 className={styles['product-name']}>{item.name}</h2>
                <p className={styles['product-storage']}>Storage: {item.storage} GB</p>
                <p className={styles['product-price']}>Price: {item.price * getTotalItemCountInCart(item.id)} грн</p>
                <div className={styles['item-counters']}>
                  <button
                    onClick={() => handleDecrease(item)}
                    className={`${styles['counter-button']} ${getTotalItemCountInCart(item.id) <= 1 ? styles['disabled'] : ''}`}
                    disabled={getTotalItemCountInCart(item.id) <= 1}
                  >
                    -
                  </button>
                  <span className={styles["counter"]}>{getTotalItemCountInCart(item.id)}</span>
                  <button
                    onClick={() => handleIncrease(item)}
                    className={styles['counter-button']}
                  >
                    +
                  </button>
                </div>
                <button className={styles['remove-button']} onClick={() => handleRemove(item.id)}>X</button>
              </div>
            </div>
          ))}
        </div>
        <div className={styles['total-price-block']}>
          <span>
            <h1>Total Price</h1>
          </span>
          <span>
            <span className={styles['total-value']}>{totalPrice}</span> грн
          </span>
          <button className={styles['buy-now-button']}>BUY NOW</button>
        </div>
      </div>
      {showModalDelete && (
        <RenderModalDelete
          onClose={handleCloseDeleteModal}
          product={cart.find(item => item.id === itemToRemove)}
          confirmAction={handleConfirmDelete}
          isFromCartPage={true}
        />
      )}
    </>
  );
};

CartPage.propTypes = {
  products: PropTypes.array.isRequired,
};

export default CartPage;