import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { removeFromFavorites, openModalFavorite, closeModalFavorite } from '../../redux/dataSlice.js';
import FavoriteIcon from '../../component/svg/FavoriteIcon/FavoriteIcon.jsx';
import RenderModalFavorite from '../../component/RenderModalFavorite/RenderModalFavorite.jsx';
import './FavoritePage.scss';

const FavoritePage = () => {
  const dispatch = useDispatch();
  const favorites = useSelector((state) => state.data.favorites);
  const products = useSelector((state) => state.data.products);
  const selectedProduct = useSelector((state) => state.data.selectedProduct);
  const [modalOpen, setModalOpen] = useState(false);

  useEffect(() => {
    if (favorites.length === 0) {
      setModalOpen(false);
      dispatch(closeModalFavorite());
    }
  }, [favorites, dispatch]);

  const handleFavoriteClick = (productId) => {
    const isFavorite = favorites.includes(productId);
    const product = products.find(p => p.id === productId);
    if (isFavorite) {
      dispatch(removeFromFavorites(productId));
      dispatch(openModalFavorite({ product: product, isAdded: false }));
    } else {
      dispatch(openModalFavorite({ product: product, isAdded: true }));
    }
    setModalOpen(true);
  };

  const handleCloseModalFavorite = () => {
    dispatch(closeModalFavorite());
    setModalOpen(false);
  };

  const favoriteProducts = products.filter(product => favorites.includes(product.id));

  if (favoriteProducts.length === 0) {
    return (
      <div className="emptyBlock">
        <h1>Favorite Page</h1>
        <h2>You haven't added any items to the favorite yet</h2>
      </div>
    );
  }

  return (
    <div className="favorite-page">
      <h1>Favorite Page</h1>
      <div className="favorite-grid">
        {favoriteProducts.map(product => (
          <div key={product.id} className="favorite-item">
            <div className="product-image-container">
              <img src={product.image_path} alt={product.name} className="product-image" />
            </div>
            <div className="product-details">
              <h3>{product.name}</h3>
              <p>Price: {product.price} грн</p>
              <p>Article: {product.article}</p>
            </div>
            <FavoriteIcon
              className="favorite-btn"
              isFavorite={true}
              onClick={() => handleFavoriteClick(product.id, product.name)}
            />
          </div>
        ))}
      </div>
      {modalOpen && <RenderModalFavorite onClose={handleCloseModalFavorite} selectedProduct={selectedProduct} />}
    </div>
  );
};

export default FavoritePage;
