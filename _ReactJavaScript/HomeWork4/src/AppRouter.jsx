import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Routes, Route } from 'react-router-dom';
import { fetchProducts } from './redux/dataSlice.js';
import ShopPage from './routes/ShopPage/ShopPage.jsx';
import HomePage from './routes/HomePage/HomePage.jsx';
import FavoritePage from './routes/FavoritePage/FavoritePage.jsx'; 
import CartPage from './routes/CartPage/CartPage.jsx'; 
import Header from './component/Header/Header';

const AppRouter = () => {
    const dispatch = useDispatch();
    const favorites = useSelector((state) => state.data.favorites);
    const cart = useSelector((state) => state.data.cart);
    const products = useSelector((state) => state.data.products);

    useEffect(() => {
        dispatch(fetchProducts());
    }, [dispatch]);

    return (
        <>
            <Header />
            <Routes>
                <Route path='/' element={<HomePage />} />
                <Route path='/shop' element={
                    <ShopPage
                        favorites={favorites}
                        cart={cart}
                        products={products}
                    />} />
                <Route path='/favorite' element={
                    <FavoritePage
                        favorites={favorites}
                        products={products}
                    />} />
                <Route path='/cart' element={
                    <CartPage
                        cart={cart}
                        products={products}
                    />} />
            </Routes>
        </>
    );
};

export default AppRouter;
