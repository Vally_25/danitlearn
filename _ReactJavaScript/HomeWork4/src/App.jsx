
import AppRouter from './AppRouter.jsx';
import './App.css';


const App = () => {
  
  return (
    <div className="app">
      <AppRouter />
    </div>
  );
};

export default App;
