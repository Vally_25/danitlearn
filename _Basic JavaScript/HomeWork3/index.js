/*
1)  Логический оператор, это оператор, который позволяет сгруппировать 2 и более
    условий в один IF для избежания многошаговой вложенности разных проверок
        пример:
            if на улице зима AND я выхожу из дома{
                надеть куртку
            }

2)  ЛОГИЧЕСКИЕ:
            && - И, код будет выполняться только тогда, когда все условия ТРУ
            || - ИЛИ, хотя-бы одно условие должно быть ТРУ
            !  - НЕ, ни одно условия не должно быть ТРУ

*/

// TASK 1
const userAge = +prompt("enter your age");
// used method "isNaN" - checked data on enteret some NUMBER.
// it comeback false if entered data is NUMBER, if comeback true - 
// need entered NUMBER data
if (isNaN(userAge)) {
    alert("Entered data is NOT a Number");
} else {
    if (userAge <= 12) {
        alert("You are Child");
    }
    else if (userAge <= 18) {
        alert("You are teenager");
    }
    else{
        alert("You are old");
    } 
}

//TASK 2
const januaryMonth = 31; const februaryMonth = 28; const marchMonth = 31;
const aprilMonth = 30; const mayMonth = 31; const juneMonth = 30;
const julyMonth = 31; const augustMonth = 31; const septemberMonth = 30;
const octoberMonth = 31; const novemberMonth = 30; const decemberMonth = 28;

let userDataMonth = prompt("Enter your Month in text format:");
switch (userDataMonth) {
    case "january":
        console.log(januaryMonth);
        break;
    case "february":
        console.log(februaryMonth);
        break;
    case "march":
        console.log(marchMonth);
        break;
    case "april":
        console.log(aprilMonth);
        break;
    case "may":
        console.log(marchMonth);
        break;
    case "june":
        console.log(juneMonth);
        break;
    case "july":
        console.log(julyMonth);
        break;
    case "august":
        console.log(augustMonth);
        break;
    case "september":
        console.log(septemberMonth);
        break;
    case "october":
        console.log(octoberMonth);
        break;
    case "november":
        console.log(novemberMonth);
        break;
    case "december":
        console.log(decemberMonth);
        break;
    default:
        console.log("Sorry, but this month not found, or you enter error month naming")
}