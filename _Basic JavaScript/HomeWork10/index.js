/* 
    1 createElement, Append, Prepend, AppendChild, PrependChild, instertBefore, insertAfter
    2. знайти елемент, викликати метод "ремув()";
    const deleteNav = document.querySelector(".navigation");
    deleteNav.remove();
    3. Append, Prepend, AppendChild, PrependChild, instertBefore, insertAfter

 */


//TASK 1
const newElementA = document.createElement('a');

newElementA.textContent = "Learn More";
newElementA.href = "#";

let searchFooter = document.querySelector('footer');

searchFooter.append(newElementA);


//TASK 2
const searchMain = document.querySelector("main");
const selectElement = document.createElement("select");
selectElement.id = "rating";

let ratings = [
    { value: '4', text: '4 Stars' },
    { value: '3', text: '3 Stars' },
    { value: '2', text: '2 Stars' },
    { value: '1', text: '1 Star' }
];

ratings.forEach((rating) => {
    const optionElement = document.createElement('option');
    optionElement.value = rating.value;
    optionElement.textContent = rating.text;
    selectElement.append(optionElement);
});

searchMain.prepend(selectElement);