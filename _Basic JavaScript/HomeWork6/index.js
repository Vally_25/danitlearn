/*
    1) функція всередені об'єкту, яку можна викликати через крапку, звертаючись 
    до самого об'єкту
    2) Булеан (если речь идет о врайтбл, енумерейбл, конфигьюрейбл) 
    немного не понял вопроса
    3) це значить, що при присвоюванні іншій змінній значення об'єкта, простір пам'яті 
    не створює новий об'єкт з тими ж параметрами, а додає посилання на вже стоврений
    перший об'єкт. приклад:

    const example = {
        some properties
    }

    const linkExample = example;

    "linkExample" зберігає посилання на об'єкт "example", і якщо звернутися 
    до властивості "linkExample[someKey]", то компілятор спочатку знайде "example", 
    потім знайде у нього таку властивість і поверне її нам, а НЕ створить новий об'єкт
    "linkExample" та НЕ буде шукати у нього цю властивіть і повертати її нам.

*/


//TASK1
const product = {
    name: "Banana",
    price: 15,
    discount: 5,
    totalPrice () {
        return product.price + product.discount;
    }
}
console.log(`Total price with discount ${product.totalPrice()}$`);




//TASK2
const getUserHello = (userName, userAge) => {
    userName = prompt("Enter your name");
    userAge = +prompt("Enter your age");
    const userData = {
        name: userName,
        age: userAge,
    }
    return `Hello, ${userData.name}, your age is ${userData.age}`
}
alert(getUserHello());



//TASK3
const productCard = {
    category: 'smartphones',
    segment: 'luxury',
    nameModel:{
        name: 'iphone 15 pro',
        used: true,
        technicalParams: ['3 cameras', 'dynamic island', 256]
    }
};

function cloningObj(typeObject){
    if (typeof typeObject !=='object'){
        return typeObject;
    };

    const cloneProperty = Array.isArray(typeObject) ? [] : {};

    for(let key in typeObject){
        if(typeObject.hasOwnProperty(key)){
            cloneProperty[key] = cloningObj(typeObject[key]);
        }
    }
    return cloneProperty;
}

const cloneProductCard = cloningObj(productCard);

console.log('Main object: ', productCard);
console.log('CLoned object: ', cloneProductCard);