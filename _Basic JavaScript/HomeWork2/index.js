/*
1)  Специальные символы(набор символов) для выполнения арифметических, 
    математических, логических операций

    оператор присваивания 
        (+=, -=, *=, /=, %=, =)
    оператор сравнения 
        (==, ===, !=, !==, >, <, <=, >=)
    арифметические операторы 
        (%, ++, --, -, +, **) 

2)  Операторы сравнения используют для проверки выполнения 
    условий для дальнейшего выполнения скрипта
        пример: сверить пароль, который ввел пользователь, с паролем, 
    который хранится внутри программы

3)  Операция присваивания - это операция, при которой переменная получает 
    значение для дальнейшей работы
        пример: 
            let a = 5;
            const password = "admin";
            let a, b;
            a = b = 5;
*/

// TASK 1

//variables for saving data
let userName = "Valerii";
const password = "secret";
//variable for saving entered data
let userPrompt = prompt("enter your pass: ");
//variable for flag entered data and password
let isAuthorization = false;

console.log(userName, password);
/*  comparison of entered data with the password variable 
    and output of the verification result   */
if(userPrompt === password){
    console.log(isAuthorization = true);
}
else{
    console.log(isAuthorization);
}

//TASK 2
let x = 5;
let y = 3;
alert(`${x+y}, ${x-y}, ${x*y}, ${x/y}`);
