/*Теоретичні питання
    1) setInterval - робить дію циклічно через певний проміжок часу.
       setTimeout - робить дію 1 раз через певний проміжок часу
    2) використати функцію clearInterval передавши як аргумент
    назву сет Таймауту/інтервалу(countdownInterval);
*/

//TASK1 
let changeTextDivBlock = document.querySelector('div');
changeTextDivBlock.textContent = "default text";
changeTextDivBlock.style.color = "red";
document.querySelector('#btn').addEventListener('click', function() {
    setTimeout(function() { 
        changeTextDivBlock.textContent = 'Access!';
        changeTextDivBlock.style.color = "green";
    }, 3000);
});

//TASK2
let backTimer = document.querySelector('#backTimer');
let count = 10;
backTimer.textContent = count;

const countdownInterval = setInterval(function () {
    count--;
    backTimer.textContent = count;
    if (count === 0) {
        clearInterval(countdownInterval);
        backTimer.textContent = 'back counter DONE';
        backTimer.style.color = "green";
    }}, 1000); 