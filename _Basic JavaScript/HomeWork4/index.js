/*
    1) цикл - структура, яка дозволяє виконувати код
    декілька разів, поки не задовольниться умова
    2) покроковий (FOR)
       передумова (WHILE)
       постумова (DO...WHILE)
    3) While - сначала проверяется условие, и если оно истина
    то тело цикал выполняется, если ложно то не выполняется НИ РАЗУ
    DO WHILE - выполнит код, потом будет проверка условия.
    одна итерация при фолс будет выполнена.
*/

//TASK 1
let firstUserNumber;
let secondUserNumber;

do {
    firstUserNumber = +prompt("enter first number");
    secondUserNumber = +prompt("enter second number");

} while (isNaN(firstUserNumber) && isNaN(secondUserNumber));

if (firstUserNumber < secondUserNumber) {
    for (let i = firstUserNumber; i <= secondUserNumber; i++) {
        console.log(i);
    }
} else if (firstUserNumber > secondUserNumber) {
    for (let i = secondUserNumber; i <= firstUserNumber; i++) {
        console.log(i);
    }
}

//TASK 2

let thirdSecondNumber;

do {
    thirdSecondNumber = +prompt("Enter your number");
} while (thirdSecondNumber % 2 !== 0);

