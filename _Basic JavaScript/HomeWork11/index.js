/* 
1.події використовуються для відслідковування дій користувача на сайті 
(кліки по формі, скроли, тощо)
2. маусмув, клік, дабблклік, райтклік
3. подія, яка дозволяє відкрити контекстне меню, здебільшого, права кнопка миші
 */


// TASK 1
const buttonElement = document.querySelector("#btn-click");
const sectionElement = document.querySelector("#content");
const paragraphElement = document.createElement("p");
paragraphElement.id = "created-paragraph";

buttonElement.addEventListener("click", () => {
    divElement.prepend(paragraphElement);
    paragraphElement.innerText = "New Paragraph";
});

//TASK 2
const divElement = document.createElement("div");
sectionElement.append(divElement);

const buttonCreateElement = document.createElement("button");
buttonCreateElement.id = "btn-input-create";
buttonCreateElement.innerText = "add input";
divElement.appendChild(buttonCreateElement);

buttonCreateElement.addEventListener("click", () => {
    const createInputElement = document.createElement("input");
    createInputElement.type = "phone";
    createInputElement.placeholder = "enter your phone";
    const divElem = document.createElement("div");
    divElement.append(divElem);
    divElem.append(createInputElement);
})