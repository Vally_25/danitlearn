/*
    1) створити змінну з рядком, отримати через промтп, перевести 
    будь-який примітив через туСтрінг.
    2) одинарні та подвійні лапки можна обирати для визнчення рядків в залежності 
    від стилістики написання коду.
        зворотні лапки - це лапки шаблонні, в них можна вказувати ДжаваСкрипт код
        якщо звертатись до нього через конструкцію "${}". тоді все, що всередині такої
    конструкції буде компілюватися у валідний код
    3) є метод localCompare(); стр1.ЛокалКомпэйр(стр2) - к уьому псевдокоды звертання
    выдбуваэться наступним чином: ми беремо стр1 і викликаємо метод порівняння 
    локалКомпейр де параметром передаємо стр2. порівнювати можна як довжину рядка, так і
    алфавітний порядок символів у рядку
    4) кількість мілісекунд, які пройшли з 1 січня 1970 року
    5) дейт.нау повертає значення в мілісекундах з початку епохи юнікс,
    не створює новий об'єкт типу дейт.
        нью дейт - це конструктор дати, він створює об'єкт типу дейт, може отримувати 
    аргументом рядок
*/




//TASK1
let str = prompt("enter your string").replaceAll(" ", "").toLowerCase();
let strReverse = str.split("").reverse().join("");
function isPalindrome(){
    if (str === strReverse){
        return true;
    } else{
        return false;
    }
}
console.log(isPalindrome());


//TASK2

const strUser = prompt("enter your string: ");

function getLengthString(maxLengthString){
    if(strUser.length <= maxLengthString){
        return true
    }else{
        return false;
    }
}

console.log(getLengthString(10));


//TASK3

const userBirthDate = prompt("Enter your birthdate (format: YYYY-MM-DD)");

function calculateAge() {
    
    let birthDate = new Date(userBirthDate);
    let currentDate = new Date();
    let age = currentDate.getFullYear() - birthDate.getFullYear();


    if (currentDate.getMonth() < birthDate.getMonth() || 
        (currentDate.getMonth() === birthDate.getMonth() && 
        currentDate.getDate() < birthDate.getDate())) {
        age--;
    }

    return age;
}
console.log(`You are ${calculateAge()} year old`);
