/*
1)  var - variable, устарелый формат
    let - изменяемая переменная
    const - константа, неизменяемая переменная

2)  string - тип данныйх, сохраняющий строку(набор символов)
        let name = "John" let surname = "Johnson"

3)  использовать зарезервированное слово typeof при выводе переменной

4)  1 + '1' = 11 - Джаваскрипт язык динамической типизации, при
сложении числа со строкой джэс автоматически переводи число в строку
и производит конкатенацию(объединение) строк. 
*/
let distance = 342;
console.log(distance, typeof distance);

let myName = 'Valerii';
let lastName = 'Bulhakov';
console.log(`My name is ${myName} ${lastName}`);
console.log(`And i ride ${distance}km on the car`);
