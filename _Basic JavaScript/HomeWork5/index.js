/*
    1) const functionName = (some parameters, or empty) => {body of function}
       function someName(some parameters, or empty){body of function}
       let nameVariable = function (some parameters, or empty){body of function}

       functionName();
       someName();
       nameVariable();

    2) коли ми створюємо функцію, вона може виконувати будь-що, але значення треба повернути, для того щоб ними можна було оперувати, тому метод РЕТУРН допомагає нам повернути значення виконання дій функції. після цих дій, треба написати РЕТУРН та назви тих значеннь, які нам треба повернути.

    3) Параметр - значення, які функція отримує на на етапі створення(приклад капсом):

        function someName(PARAM_1,PARAM_2,..,PARAM_N){
            some body
        }

       Аргумент - значення, що отримує функція на етапі виклику
        someName(SOMEARGUMENT_1, SOMEARGUMENT_2,..,SOMEARGUMENT_N);

    4) на етапі виклику функції номер 2, як аргумент передати назву
       функції номер 1 але без круглих дужок (приклад капсом):

        const someFunc_1 = () => {};
        const someFunc_2 = () => {};

        someFunc_2(SOMEFUNC_1);   
        
        або можна виконати функції, потім зробити додатковий крок у вигляді присвоювання, а потім викликати:
            const someFunc_1 = () => {};
            const someFunc_2 = () => {};

            let someVariable = someFunc_1;
            
            someFunc_2(someVariable);

*/
//TASK 1
const calcTwoNumber = (a, b) => a / b;
/*function calcTwoNumber(a, b){
    return  a / b;
}*/
console.log(`Answer for FIRST TASK: ${calcTwoNumber(32, 2)}`);



//TASK 2
    //функция проверяет знак на соответствие 
    //знакам операции, если не соответствует - алерт.
    //так же он возвращает ТРУ если знак из свитча, и фолс, 
    //если нет 

const checkUserSymbol = (userSymbol) => {
    switch (userSymbol) {
        case "+":
        case "-":
        case "*":
        case "/":
            return true;
        default:
            alert("Missing or invalid math operation symbol");
            return false;
    }
}

    //функция проверяет введеный знак на соответствие 
    //с функцией проверки и возвращает значение

const getUserSymbol = () => {
    let userMathSymbol;
    do {
        userMathSymbol = prompt("Enter your symbol");
    } while (!checkUserSymbol(userMathSymbol));
    return userMathSymbol;
}

    //функция валидирует введеное значение на ЧИСЛО.
    //если НЕ ЧИСЛО или равное нулю, запрашивает число 
    //до тех пор, пока не будет ЧИСЛО

const getValidatedNumber = (message = "not a number") => {
    let numberUser = +prompt(message);
    while (isNaN(numberUser) || numberUser === 0) {
        numberUser = +prompt(message);
    }
    return numberUser;
}

    //функция получает все данные пользователя 
    //и возвращает их. возврат происходит с 
    //переприсваиванием к другим переменным

const getUserData = () => {
    const numberUser1 = getValidatedNumber("Enter first Number:");
    const numberUser2 = getValidatedNumber("Enter second Number:");
    const symbolUser = getUserSymbol();
 //использую переприсваивание через "Ключ-Значение"
 //объектов
    return {
        number1: numberUser1,
        number2: numberUser2,
        symbol: symbolUser
    };
}

    //функция получает все данные и проводит мат.
    //операции согласно заданию

const calculateUserData = (userData) => {
    switch (userData.symbol) {
        case "+":
            return userData.number1 + userData.number2;
        case "-":
            return userData.number1 - userData.number2;
        case "*":
            return userData.number1 * userData.number2;
        case "/":
            return userData.number1 / userData.number2;
        default:
            return "Invalid math operation symbol";
    }
}

 //вызов функций для заполнения данных и калькуляции
const userData = getUserData();
const result = calculateUserData(userData);
 //вывод результата
console.log("Answer for SECOND TASK:", result);


//TASK 3

const calcFactorial = (factorialUser) =>{
    return factorialUser ? factorialUser * calcFactorial(factorialUser - 1) : 1;
}
console.log("Answer for THIRD TASK: ", calcFactorial(+prompt("Enter your number for calculating factorial:")));