/* 
    1) localStorage - зберігає дані на пристрої користувача, доки їх не видалити
	або пока не закінчиться термін дії кешу
	   sessionStorage - зберігає дані допоки вкладка або вікно браузеру не закриється

    2) не зберігати паролі у відкритому доступі(завжди шифрувати)
		SSL\TLS - сертифікати
		HTTPS - захищене з'єднання
		обмеження доступу до даних для сторонніх скриптов
		використовувати токени
    3) при повторному відкритті браузера після закриття створиться нова сесія
	тому дані очистяться
*/

const root = document.documentElement;
const changeThemeButoon = document.querySelector('#theme');
const isThemeActive = localStorage.getItem('isThemeActive');

if (isThemeActive === 'true') {
	changeThemeButoon.classList.add('active');
	root.style.setProperty('--bg-color', '#242424');
	root.style.setProperty('--text-color', '#F6FDFF');
	root.style.setProperty('--bg-color-price-plan', '#444444');
}

changeThemeButoon.addEventListener('click', (event) => {
	event.target.classList.toggle('active');

	if (event.target.classList.contains('active')) {
		root.style.setProperty('--bg-color', '#242424');
		root.style.setProperty('--text-color', '#F6FDFF');
		root.style.setProperty('--bg-color-price-plan', '#444444');
	} else {
		root.style.setProperty('--bg-color', '#fff');
		root.style.setProperty('--text-color', '#0E0F13');
		root.style.setProperty('--bg-color-price-plan', '#FDFDFD');
	}

	localStorage.setItem('isThemeActive', event.target.classList.contains('active'));
})
