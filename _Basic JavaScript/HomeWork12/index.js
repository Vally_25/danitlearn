/* 
    1. за допомогою метода КЕУ : 
        window.addEventListener("keydown", (event) => {
        console.log(event.key);
        але треба включити прослуховувач де буде вказано або кейАп, або кейДаун.
    2. ивент.код показує саме код клавіші, і він буде однаковий без залежності від 
    натиснутого шрифту, розкладки клавіатури тощо.
    ивент.кей вказує НАЗВУ кнопки, і вона буде різною, в залежності від регістру: 
    Enter, KeyQ, тощо.
    3.keyDown, KeyUp, KeyPress - 
    keyDown - спрацює, коли клавіша натиснута, якщо зажати, буде багато виконань.
    KeyUp - спрацює лише один раз, коли клавішу відпустити
    KeyPress - спрацює коли клавіша буде натиснута і відпущена
    (комбінація keyDown та KeyUp). спрацює один раз
*/

const buttonElements = document.querySelectorAll(".btn");

window.addEventListener("keydown", (event) => {
    const key = event.key.toLowerCase();

    buttonElements.forEach((element) => {
        const buttonKey = element.innerText.toLowerCase();
        if (buttonKey === key) {
            buttonElements.forEach((btn) => btn.classList.remove("active"));
            element.classList.add("active");
        }
    });
});
