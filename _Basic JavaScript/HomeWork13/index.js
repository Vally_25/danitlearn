/* 
    1. для того щоб змінити базову поведінку "сабміт". за замовчуванням сабміт
    відправляє кудись на сервер дані з форми. відмінивши цю поведінку, ми можемо
    задати свою
    2. у тому, що один обробник подій буде виконувати різні дії з різними елементами
    на сторінці, у дочірніх елементаї, сам обробник подій буде на якомусь вищому, 
    батьківському, рівні (наприклад обробник подій на увесь документ, а дії з об'єктами
    на сторінці.)
    3. 
    Події документу:
    DOMContentLoaded: спрацює, коли сторінка повністю завантажена.
    click: клік по елементу.
    keydown, keyup: клік по клавіші.
    submit: при відправки форми.
    change: при зміні значення у елемента форми.
    Події вікна браузера:
    load: повне завантаження всіх ресурсів.
    unload: спрацює перед залишенням сторінки.
    resize: при зміні розміру вікна браузера.
    scroll: при прокрутці сторінки.
    beforeunload:спрацює перед виходом користувача з сторінки.
    використовується для попереджень щодо незбережених данних.
*/


const tabsElement = document.querySelector('.tabs');
const tabsList = document.querySelectorAll('.tabs-title');
const tabsItems = document.querySelectorAll('.tabs-content li');

tabsElement.addEventListener('click', (event) => {
    const activeElem = event.target;

    if (activeElem.classList.contains('tabs-title')) {
        const tabName = activeElem.getAttribute('data-tab');
        tabsList.forEach((elem) => {
            elem.classList.remove('active')
        });
        tabsItems.forEach((elem) => {
            elem.classList.remove('active')
        });
        activeElem.classList.add('active');
        document.querySelector(`.tabs-content li[data-tab="${tabName}"]`).classList.add('active');
    }
});