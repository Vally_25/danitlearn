/*
    1)модель, де кожний елемент ХТМЛ документу є об'єктом
    до якого можна звернутися, додати/видалити/змінити 
    параметри чи викликати глобальні та локальні методи
    2) ІннерХТМЛ додає ХТМЛ текст, іннерТЕКСТ додає текст
    в створений або обраний тег хтмл
    3) getElement(-s)By*type of attribute та
       querySelector(-All).
       останній має можливість перебору за допомогою 
       функції форІч, тому для перебору елементів краще 
       кверіселектор. 

    *ID, CLASS or TagName
    4)
    НодЛіст частіше містить різні типи вузлів, включаючи елементи, текстові вузли, коментарі тощо.
    ХТМЛколекшн зазвичай містить тільки елементи.
*/

//TASK 1

const searchMethodClassName = document.getElementsByClassName("feature");
console.log(searchMethodClassName);
const searchMethodQuery = document.querySelectorAll(".feature");
console.log(searchMethodQuery);

searchMethodQuery.forEach((elem) => {
    elem.style.textAlign = "center";
})


//TASK 2

const changeH2content = document.querySelectorAll("h2");
console.log(changeH2content);
changeH2content.forEach((elem) => {
    elem.textContent = "Awesome feature";
})

//TASK 3
const changeFeatureTitlecontent = document.querySelectorAll(".feature-title");
console.log(changeFeatureTitlecontent);
changeFeatureTitlecontent.forEach((elem) => {
    console.log(elem.textContent);
    elem.textContent = `${elem.textContent}!`;
})