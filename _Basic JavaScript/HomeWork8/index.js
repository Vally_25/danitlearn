/*
    1) для кожного об'єкту массива виконає колбек функцію, яка вказана у тілі.
    2) Методи, що змнюють масив:
        pop - видаляє eлем масиву у кінці arr.pop();
        push - додає eлем масиву у кінці, необхідне значення arr.push(value);
        shift - видаляє eлем масиву у початку arr.shift();
        unshift - додає eлем масиву у початку, необхідне значення arr.unshift(value);
        splice - Додає, видаляє або замінює елем всередині масиву, 
        потрібне значення arr.splice(value);
       Методи, що повертають новий:
        concat - об'єднує масиви, як значення викликається інший масив:
            ar1 = [1,2]; ar2 = [3,4]
            ar3 = ar1.concat(ar2);//[1,2,3,4]
        slice - повертає частину масиву, як новий. порожні скобки повертають весь масив:
            ar1 = [1,2,3,4];
            ar2 = ar1.slice(1,3);// [2,4]
            ar3 = ar1.slice();//[1,2,3,4]
        map - застосовує функцію до кожного елем вхідного масиву:
            ar1 = [1,2,3,4];
            ar2 = ar1.map(function(elem){
                return someAction
            })
    3)
    4)
 */


//TASK1

let arr = ["travel", "hello", "eat", "ski", "lift"];
let sumString = 0;

arr.forEach((elem) => {
    if(elem.length > 3){
        sumString++;
    }
});
console.log(sumString);


//TASK2

let userList = [
    {
        name: "Ivan",
        age: 25,
        sex: "male"
    },
    {
        name: "Mariya",
        age: 15,
        sex: "female"
    },
    {
        name: "Liya",
        age: 39,
        sex: "female"
    },
    {
        name: "Kan",
        age: 9,
        sex: "male"
    },
]

let filteredMaleUserList = userList.filter((sexType) => {
    return sexType.sex === 'male';
});
console.log(filteredMaleUserList);


//TASK3

let array = [
    "MyName is SlimSHady",
    2023,
    "2023",
    undefined,
    [3,2,1],
    null,
    undefined,
    true,
    {
        name:"Oli",
        surname:"Sykes",
    },
    true,
    false,
    "false",
    {
        name:"Slim",
        surname:"Shady",
    },
    [1,2,3],
];

function filterBy(arr, typeData) {
    return arr.filter((item) => {
        return typeof item === typeData;
    });
}
let result = filterBy(array, "object");

console.log(result);
