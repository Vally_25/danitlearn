import Modal from './Modal.js';

class LoginModal extends Modal {
  constructor(confirmCallback) {
    super(confirmCallback)
  }

  configure() {
      super.configure();

      this.title.innerText = 'Please enter your email and password';

      const form = document.createElement('form');
      const loginTemplate = document.querySelector('#login-form-template');
      form.append(loginTemplate.content.cloneNode(true));

      this.body.append(form);

      this.button.innerText = 'Login';
  }
}

export default LoginModal;
