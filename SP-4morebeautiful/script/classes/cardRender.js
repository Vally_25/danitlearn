import Modal from './Modal.js';
import modalInstance from '../script.js';


class Card extends Modal {
  constructor(formData, selectedDoctor, receivedId, confirmCallback, title) {
    super(confirmCallback);
    this.title = title;
    this.patientName = formData['patient-name'] || '';
    this.aimVisit = formData['aim-visit'] || '';
    this.descriptionVisit = formData['description-visit'] || '';
    this.urgency = formData['urgency'] || '';
    this.lastVisitDate = formData['last-visit-date'] || '';
    this.bloodPressure = formData['blood-pressure'] || '';
    this.age = formData['age'] || '';
    this.cardioDeseases = formData['cardio-deseases'] || '';
    this.bmi = formData['bmi'] || '';
    this.selectedDoctor = selectedDoctor || '';
    this.receivedId = receivedId || '';
    this.cards = [];

    this.patientNameInput = document.createElement('input');
    this.patientNameInput.type = 'text';
  }


  async waitForFormFields(form) {
    return new Promise((resolve) => {
      const interval = setInterval(() => {
        const patientNameInput = form.querySelector('#patient-name');
        const aimVisitInput = form.querySelector('#aim-visit');
        const descriptionVisitInput = form.querySelector('#description-visit');
        const urgencyInput = form.querySelector('#urgency');
        const lastVisitDateInput = form.querySelector('#last-visit-date');
        const bloodPressureInput = form.querySelector('#blood-pressure');
        const ageInput = form.querySelector('#age');
        const cardioDeseasesInput = form.querySelector('#cardio-deseases');
        const bmiInput = form.querySelector('#bmi');

        if (
          patientNameInput &&
          aimVisitInput &&
          descriptionVisitInput &&
          urgencyInput &&
          lastVisitDateInput &&
          bloodPressureInput &&
          ageInput &&
          cardioDeseasesInput &&
          bmiInput &&
          patientNameInput.value &&
          aimVisitInput.value &&
          descriptionVisitInput.value &&
          urgencyInput.value &&
          lastVisitDateInput.value &&
          bloodPressureInput.value &&
          ageInput.value &&
          cardioDeseasesInput.value &&
          bmiInput.value
        ) {
          clearInterval(interval);
          this.patientName = patientNameInput.value;
          this.aimVisit = aimVisitInput.value;
          this.descriptionVisit = descriptionVisitInput.value;
          this.urgency = urgencyInput.value;
          this.lastVisitDate = lastVisitDateInput.value;
          this.bloodPressure = bloodPressureInput.value;
          this.age = ageInput.value;
          this.cardioDeseases = cardioDeseasesInput.value;
          this.bmi = bmiInput.value;
          resolve();
        }
      }, 100);
    });
  }

  async deleteCard() {
    try {
      if (!this.receivedId) {
        console.log("delCarFirst: ", this.receivedId);
        console.error('Помилка при видаленні карточки: Не вказано receivedId');
        return;
      }
      // Викликаємо метод deleteCardFromServer для видалення карточки на сервері
      await this.deleteCardFromServer(this.receivedId);
    } catch (error) {
      console.error('Помилка при видаленні карточки:', error);
    }
  }

  async deleteCardFromServer(receivedId) {
    try {
      const response = await axios.delete(`https://ajax.test-danit.com/api/v2/cards/${this.receivedId}`, {
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      });

      if (response.status === 200) {
        console.log('Карточка успішно видалена');
        console.log("deleteCardFromReceivedID: ", this.receivedId);
      } else {
        console.error('Помилка при видаленні карточки: Неправильний статус відповіді від сервера.');
      }
    } catch (error) {
      console.error('Помилка при видаленні карточки:', error);
    }
  }

  renderCard(cardContainer) {

    const cardData = {
      patientName: this.patientName,
      aimVisit: this.aimVisit,
      descriptionVisit: this.descriptionVisit,
      urgency: this.urgency,
      lastVisitDate: this.lastVisitDate,
      bloodPressure: this.bloodPressure,
      age: this.age,
      cardioDeseases: this.cardioDeseases,
      bmi: this.bmi,
      selectedDoctor: this.selectedDoctor,
      receivedId: this.receivedId,
    };
    let searchInput = document.getElementById("search");
    if (cardData.patientName.toLowerCase().includes(searchInput.value.toLowerCase().trim())) {
      // Создаем HTML элементы для карточки
      const cardTemplate = document.querySelector('#card-template').content.cloneNode(true);
      this.card = cardTemplate.querySelector('.card');
      this.card.querySelector('.patient-name').textContent = this.patientName;
      this.card.querySelector('.doctor-name').textContent = this.selectedDoctor;

      // Добавляем обработчик события для кнопки закрытия карточки
      this.closeBtn = this.card.querySelector('.close-btn');

      this.closeBtn.onclick = async (event) => {
        event.stopPropagation(); // Остановка всплытия события, чтобы не вызывать обработчик события на карточке

        // Вызов диалога подтверждения
        const confirmed = window.confirm('Вы уверены, что хотите удалить эту карточку?');

        if (confirmed) {
          // Если пользователь подтвердил удаление
          await this.deleteCard(this.receivedId);
          this.cardContainer.removeChild(this.card); // Удаление карточки из контейнера после закрытия
        } else {
          // Если пользователь отменил удаление
          console.log('Удаление отменено');
        }
      };

      this.cardContainer = document.querySelector('.cardContainer');
      this.cardContainer.appendChild(this.card);
      const editButton = document.createElement('button');
      editButton.textContent = 'Редагувати';
      editButton.classList.add('btn', 'btn-warning', 'js-modal-warning-btn', 'createBTN');

      this.card.onclick = async () => {
        try {
          const modalFormFields = new Modal(() => { });
          modalFormFields.configure();
          modalInstance.show();

          const cardData = await this.getCardDetails(this.receivedId);

          modalFormFields.body.innerHTML = '';
          //modalFormFields.title.innerHTML = 'Інформація про прийом';

          const existingEditButton = modalFormFields.modal.querySelector('.btn-warning');
          const modalTitle = document.createElement('h3');
          const modalContent = document.createElement('div');
          modalContent.classList.add('cardValue');
          let title = document.querySelector(".modal-title");

          if (!existingEditButton) {
            this.editButton = document.createElement('button');
            this.editButton.classList.add('btn', 'btn-warning');
            this.editButton.textContent = 'Редагувати';
            modalFormFields.modal.querySelector('.modal-footer').prepend(editButton);

            this.editButton = editButton;
          }

          if (title && title.textContent !== 'Інформація про прийом') {
            title.textContent = 'Інформація про прийом';
          }

          for (let key in cardData) {
            // Проверяем ключ на имя пациента
            if (key !== 'id') {
              if (key === 'patient-name') {
                modalTitle.textContent = `${key}: ${cardData[key]}`;
                modalContent.prepend(modalTitle);
              } else {
                const label = document.createElement('label');
                label.textContent = `${key}: ${cardData[key]}`;
                // const labelStatus = document.createElement('label');
                // labelStatus.textContent = "Status:"
                modalContent.appendChild(label);
                // modalContent.appendChild(labelStatus);
              }
            }
          }

          modalFormFields.body.appendChild(modalContent);

          this.button.textContent = "Save";
          // НАН логику прописать, чтобы исключить дублирования
          this.editButton.onclick = () => {
            // Очищаем содержимое modalContent перед добавлением инпутов
            modalContent.innerHTML = '';
          
            // Проходим по всем полям в cardData
            for (const key in cardData) {
              if (key !== 'id') { // Пропускаем поле 'id'
                // Создаем новый инпут для каждого поля
                const inputElement = document.createElement('input');
                inputElement.type = 'text';
                inputElement.name = key; // Устанавливаем имя поля в инпут
          
                // Устанавливаем текущее значение поля в инпут
                inputElement.value = cardData[key];
          
                // Создаем новый label для каждого поля
                const labelElement = document.createElement('label');
                labelElement.textContent = `${key}: `;
          
                // Добавляем инпут и label в modalContent
                modalContent.appendChild(labelElement);
                modalContent.appendChild(inputElement);
                modalContent.appendChild(document.createElement('br')); // Добавляем перенос строки для разделения полей
              }
            }
          };

          const inputElements = {
            patientName: document.querySelector('#patient-name'),
            aimVisit: document.querySelector('#aim-visit'),
            descriptionVisit: document.querySelector('#description-visit'),
            urgency: document.querySelector('#urgency'),
            lastVisitDate: document.querySelector('#last-visit-date'),
            bloodPressure: document.querySelector('#blood-pressure'),
            age: document.querySelector('#age'),
            cardioDeseases: document.querySelector('#cardio-deseases'),
            bmi: document.querySelector('#bmi')
          };

          this.button.onclick = () => {
            const updatedData = {}; // Инициализируем объект updatedData
          
            // Проверяем каждое поле перед добавлением в updatedData
            for (const key in inputElements) {
              const inputElement = inputElements[key]; // Получаем соответствующий инпут
          
              // Проверяем, было ли изменено значение поля или оставлено пустым
              if (inputElement && (inputElement.value !== cardData[key] || inputElement.value.trim() === '')) {
                updatedData[key] = inputElement.value; // Добавляем значение инпута в updatedData по ключу
              }
            }
          
            // Отправляем обновленные данные на сервер
            axios.put(`https://ajax.test-danit.com/api/v2/cards/${this.receivedId}`, updatedData, {
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
              }
            })
              .then(response => {
                if (response.status === 200) {
                  console.log('Данные успешно обновлены:', response.data);
                  modalInstance.hide();
                } else {
                  throw new Error('Ошибка при обновлении данных о карте');
                }
              })
              .catch(error => {
                console.error('Ошибка при обновлении данных:', error);
              });
          };
        } catch {
          console.error('Ошибка при обновлении данных:');
        }
      }
    }
  }

  async getCardDetails(receivedId) {
    try {
      const response = await axios.get(`https://ajax.test-danit.com/api/v2/cards/${receivedId}`, {
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      });

      if (response.status === 200) {
        return response.data;
      } else {
        throw new Error('Ошибка при получении данных о карте');
      }
    } catch (error) {
      throw new Error('Ошибка при получении данных о карте:', error);
    }
  }

  ///ДОЛЖЕН ВЫЗЫВАТЬСЯ КОД В ОНЛОАД или аналогично...
  static async fetchCardsAndRender(cardContainer) {
    try {
      const response = await axios.get('https://ajax.test-danit.com/api/v2/cards', {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      });

      this.cards = response.data;
      if (this.cards.length === 0) {
        cardContainer.textContent = 'Not yet cards'; // Если сервер пустой, выводим сообщение
      } else {
        this.cards.forEach(cardData => {
          let { selectedDoctor, receivedId, ...formData } = cardData;
          receivedId = cardData.id;
          const cardInstance = new Card(formData, selectedDoctor, receivedId); // Создаем экземпляр карточки
          cardInstance.renderCard(cardContainer); // Рендерим карточку в контейнере
        });
      }
      return this.cards; // Возвращаем массив карточек
    } catch (error) {
      console.error('Ошибка при загрузке карточек:', error);
      return []; // Возвращаем пустой массив в случае ошибки
    }
  }


  // changeCard(){

  // }
  // waitServerResponse(){
  //   axios.get("..link/cars/"){
  //     docum4.querySelector('.cardCOntainer');
  //   }
  // }

}

export default Card;