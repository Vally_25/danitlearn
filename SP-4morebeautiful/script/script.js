"use strict";

/*
ed23f650-a16b-4566-aa18-94d2b72431f4 - my token "Valerii"
*/
/*IMPORTS ANOTHER CONNECTION MODULES*/
// import LoginModal from "./classes/LoginModal.js";
// import loginRequest from "./api/loginRequest.js";
// import AddComponentModal from "./classes/AddComponentModal.js";
/*import LoginModal from "./classes/LoginModal.js";
import loginRequest from "./api/loginRequest.js";
import AddComponentModal from "./classes/AddComponentModal.js";*/

/*ALL DOCUMENTS SEARCHING*/
const login = document.querySelector('.js-login');
const createCardBtn = document.querySelector('.js-create-card');
const logout = document.querySelector('.logout');

let searchInput = document.getElementById("search");
const searchOpen = document.getElementById("statusOpen").value;
const searchDone = document.getElementById("statusDone").value;
const filterHigh = document.getElementById("priorityHigh").value;
const filterNormal = document.getElementById("priorityNormal").value;
const filterLow = document.getElementById("priorityLow").value;

const clearBtn = document.querySelector("#clearFilter");
const clearServer = document.querySelector("#clearServer");




//TEST CODE

//GET TOKEN AUTHORIZATION /**/
axios.post('https://ajax.test-danit.com/api/v2/cards/login', {
  email: 'snieznapanda@gmail.com',
  password: 'ктки18!',
}
  , {
    headers: {
      'Content-Type': 'application/json'
    }
  })
  .then(response => {
    const token = JSON.stringify(response.data);
    console.log(token);
  })
  .catch(error => {
    console.error('Error:', error);
  });

import LoginModal from "./classes/LoginModal.js";
import Card from "./classes/cardRender.js";
import loginRequest from "./api/loginRequest.js";
import AddComponentModal from './classes/AddComponentModal.js';

const modalInstance = new bootstrap.Modal('#modal', {
  keyboard: false
})

if (localStorage.getItem('token')) {
  console.log('localStorage');
  modalInstance.hide();
  login.style.display = 'none';
  createCardBtn.style.display = 'block';
  logout.style.display = 'block';
}
else {

  login.addEventListener('click', () => {
    new LoginModal(loginModaHandler).configure()
  })

  const loginModaHandler = async (modal) => {
    console.log('loginModaHandler');
    const formData = modal.formSerialize();

    const data = await loginRequest(formData);
    if (data) {
      localStorage.setItem('token', data);
      modalInstance.hide();
      login.style.display = 'none';
      createCardBtn.style.display = 'block';
      logout.style.display = 'block';
    }
    else {
      alert("Неправильно введені дані")
    }
  }
}
logout.addEventListener('click', () => {
  localStorage.removeItem('token');
  modalInstance.hide();
  login.style.display = 'block';
  createCardBtn.style.display = 'none';
  logout.style.display = 'none';


})

createCardBtn.addEventListener('click', () => {
  const modalFormFields = new AddComponentModal(() => { });
  modalFormFields.configure();
  modalInstance.show(); // Показываем модальное окно

  const editButton = document.querySelector('.btn-warning'); // Находим кнопку редактирования в модальном окне

  // Проверяем, если тайтл модального окна не равен 'Інформація про прийом', удаляем кнопку редактирования
  const title = modalFormFields.modal.querySelector('.modal-title');
  if (title && title.textContent !== 'Інформація про прийом' && editButton) {
    modalFormFields.modal.querySelector('.modal-footer').removeChild(editButton);
  }
});


clearBtn.addEventListener('click', async () => {
  try {
    const cardContainer = document.querySelector('.cardContainer');
    cardContainer.innerHTML = ''; // Очищаем контейнер перед рендером

    const cards = await Card.fetchCardsAndRender(cardContainer); // Загружаем и рендерим карточки
    console.log('Загруженные карточки:', cards);
  } catch (error) {
    console.error('Ошибка при очистке и рендеринге карточек:', error);
  }
});

// Добавляем обработчики событий для фильтров или ввода информации в инпут
// В зависимости от изменений вызываем методы фильтрации и рендеринга карточек
searchInput.addEventListener('input', async () => {
  const cardContainer = document.querySelector('.cardContainer');
  cardContainer.innerHTML = '';

  const searchValue = searchInput.value ? searchInput.value.toLowerCase().trim() : ''; // Проверяем наличие значения в инпуте

  // Создаем экземпляр класса Card
  const cardInstance = new Card();
  // Получаем карточки и сохраняем их в свойстве cards
  await cardInstance.fetchCardsAndRender();

  // Фильтруем карточки по имени пациента
  const filteredCards = cardInstance.cards.filter(cardData => {
    // Проверяем, что cardData существует и имеет свойство patientName
    if (cardData && cardData.patientName) {
      // Преобразуем имя пациента в нижний регистр и проверяем на вхождение в поисковый запрос
      return cardData.patientName.toLowerCase().includes(searchValue);
    }
    return false; // Возвращаем false, если cardData не существует или нет свойства patientName
  });

  // Рендерим отфильтрованные карточки
  filteredCards.forEach(cardData => {
    const { patientName, aimVisit, descriptionVisit, urgency, lastVisitDate, bloodPressure, age, cardioDeseases, bmi, selectedDoctor, receivedId } = cardData;
    const cardInstance = new Card({ 'patient-name': patientName, 'aim-visit': aimVisit, 'description-visit': descriptionVisit, 'urgency': urgency, 'last-visit-date': lastVisitDate, 'blood-pressure': bloodPressure, 'age': age, 'cardio-deseases': cardioDeseases, 'bmi': bmi }, selectedDoctor, receivedId); // Создаем экземпляр карточки
    cardInstance.renderCard(cardContainer); // Рендерим карточку в контейнере
  });
});

clearServer.addEventListener('click', async () => {
  try {
    const response = await axios.get('https://ajax.test-danit.com/api/v2/cards', {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    });

    const cards = response.data;

    cards.forEach(async cardData => {
      const cardInstance = new Card(cardData); // Створення екземпляру карти з даними, отриманими з серверу

      // Перевірка, чи порожній або невизначений receivedId карточки
      if (cardInstance.receivedId === '' || cardInstance.receivedId === undefined) {
        try {
          const response = await axios.delete(`https://ajax.test-danit.com/api/v2/cards/${cardData.id}`, {
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
          });
          if (response.status === 200) {
            console.log('Карточка успішно видалена');
          } else {
            console.error('Помилка при видаленні карточки: Неправильний статус відповіді від сервера.');
          }
        } catch (error) {
          console.error('Помилка при видаленні карточки:', error);
        }

      }

    });
  } catch (error) {
    console.error('Помилка при відправці запиту GET:', error);
  }
});


export default modalInstance;
